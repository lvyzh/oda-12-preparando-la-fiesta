﻿using System.Collections;
using UnityEngine;

namespace Assets.Resources.Scripts
{
    public class SoundManager : MonoBehaviour
    {
        //Reproductores de sonido
        [HideInInspector]
        private AudioSource[] _audios;

        private void Start()
        {
            _audios = GetComponents<AudioSource>();
            _audios[0].volume = 0.5f;
            _audios[0].loop = true;
            _audios[1].volume = 1.0f;
            _audios[2].volume = 1.0f;
            _audios[3].volume = 1.0f;
            _audios[4].volume = 1.0f;
        }

        //Manda a llamar el sonido en el audioSource indicado
        public void PlaySound(string soundName, int audioSource)
        {
            StartCoroutine(PSound(soundName, audioSource));
        }

        private IEnumerator PSound(string soundName, int audioSource)
        {
            var pathToDownload = "";
            pathToDownload = Application.platform == RuntimePlatform.Android ? Application.persistentDataPath : Application.streamingAssetsPath;
            WWW audio = new WWW("file://" + pathToDownload + "/" + soundName + ".mp3");
            yield return audio;
            try
            {
                _audios[audioSource].Stop();
                _audios[audioSource].clip = audio.GetAudioClip(true, true, AudioType.MPEG);
                _audios[audioSource].Play();
            }
            catch
            {
                Debug.Log("Fallo al reproducir el sonido");
            }
        }
    }
}