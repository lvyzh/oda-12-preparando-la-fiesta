using UnityEngine;
using UnityEngine.UI;

namespace Assets.Resources.Scripts
{
    public class InitOda12 : MonoBehaviour
    {
        private Button _btnEnter;
        private GameObject _initScreen, _mainMenu;
        private SoundManager _soundManager;
        internal void Start()
        {
            //GET Main Nodes for ODA12
            _initScreen = transform.GetChild(0).gameObject;
            _mainMenu = transform.GetChild(1).gameObject;
            //SET camera for Main Nodes
            _initScreen.transform.GetChild(0).GetComponent<Canvas>().worldCamera = Camera.main;
            _mainMenu.transform.GetChild(0).GetComponent<Canvas>().worldCamera = Camera.main;
            //GETNODES for InitScreen
            _btnEnter = _initScreen.transform.GetChild(0).FindChild("BtnEnter").GetComponent<Button>();
            _btnEnter.onClick.AddListener(delegate
            {
                _initScreen.SetActive(false);
                _mainMenu.SetActive(true);
            });
            //add SOUND 
            _soundManager = transform.GetComponent<SoundManager>();
            _soundManager.PlaySound("Tropical_Jazz_Paradise", 0);
        }
    }
}