﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.Resources.Scripts
{
    public class RetoFinalOda12 : MonoBehaviour
    {
        //Colores preguntas
        private readonly Color _answerSettedColor = new Color32(14, 209, 242, 255);
        private readonly List<Button> _btnRespuestas = new List<Button>();
        private readonly Color _defaultAnswerColor = new Color32(9, 74, 139, 255);
        private readonly List<GameObject> _popUps = new List<GameObject>();
        private readonly List<Text> _txtRespuestas = new List<Text>();
        private Button _btnAtras, _btnAceptar, _btnSalir;
        private GameObject _challengeFinal;
        private bool _iniciado;
        private MenuOda12 _menuScript;
        private int _preguntaActual;
        private List<Question> _preguntas = new List<Question>();
        private List<Answer> _respuestas = new List<Answer>();
        private SoundManager _soundManager;
        private Text _txtCalificacion, _txtRespuesta;
        private Text _txtPregunta;

        internal void Start()
        {
            _soundManager = transform.GetComponent<SoundManager>();
            //Scripts de Ayuda
            _menuScript = transform.GetComponent<MenuOda12>();

            //get node challenge final
            _challengeFinal = transform.GetChild(6).gameObject;

            //popUps
            _popUps.Add(_challengeFinal.transform.FindChild("popUpPregunta").gameObject);
            _popUps.Add(_challengeFinal.transform.FindChild("popUpResultados").gameObject);

            //popUps Buttons
            _btnAtras = _popUps[0].transform.FindChild("btnAnterior").GetComponent<Button>();
            _btnAceptar = _popUps[0].transform.FindChild("btnAceptar").GetComponent<Button>();

            _btnSalir = _popUps[1].transform.FindChild("btnSalir").GetComponent<Button>();

            var goRespuestas = _popUps[0].transform.FindChild("respuestas").gameObject;
            _btnRespuestas.Add(goRespuestas.transform.FindChild("btnRespuesta1").GetComponent<Button>());
            _btnRespuestas.Add(goRespuestas.transform.FindChild("btnRespuesta2").GetComponent<Button>());
            _btnRespuestas.Add(goRespuestas.transform.FindChild("btnRespuesta3").GetComponent<Button>());

            //popUps Text
            _txtPregunta = _popUps[0].transform.FindChild("txtPregunta").GetComponent<Text>();

            _txtRespuestas.Add(_btnRespuestas[0].transform.GetChild(0).GetComponent<Text>());
            _txtRespuestas.Add(_btnRespuestas[1].transform.GetChild(0).GetComponent<Text>());
            _txtRespuestas.Add(_btnRespuestas[2].transform.GetChild(0).GetComponent<Text>());

            _txtCalificacion
                = _popUps[1].transform.FindChild("txtCalificacion").GetComponent<Text>();
            _txtRespuesta = _popUps[1].transform.FindChild("resultadoPregunta").GetComponent<Text>();

            //Funciones Botones
            _btnAtras.onClick.AddListener(delegate { Regresar(); });
            _btnAceptar.onClick.AddListener(delegate { Avanzar(); });

            _btnSalir.onClick.AddListener(delegate
            {
                LimpiarResultados();
                _menuScript.GoToMenu();
            });

            _btnRespuestas[0].onClick.AddListener(delegate { SeleccionarRespuesta(0); });
            _btnRespuestas[1].onClick.AddListener(delegate { SeleccionarRespuesta(1); });
            _btnRespuestas[2].onClick.AddListener(delegate { SeleccionarRespuesta(2); });
            //MostrarPregunta();
        }

        internal void Update()
        {
            if (!_challengeFinal.activeSelf || _iniciado) return;
            StartCoroutine(SecuenciaEventos("iniciarEvaluacion", 0.0f));
            _iniciado = true;
        }

        // ------------------------Genericos Retos----------------------

        //Funciones secuencia
        public IEnumerator SecuenciaEventos(string secuencia, float tiempoEspera)
        {
            yield return new WaitForSeconds(tiempoEspera);
            switch (secuencia)
            {
                case "iniciarEvaluacion":
                    //sound
                    PlaySoundEvaluation();
                    CrearCuestionario();
                    SeleccionarPreguntasRandom(5);
                    _preguntaActual = 0;
                    _popUps[0].SetActive(true);
                    _popUps[1].SetActive(false);
                    LimpiarResultados();
                    MostrarPregunta();
                    break;
                case "evaluar":
                    MostrarResultados();
                    break;
                case "salir":
                    LimpiarResultados();
                    _menuScript.GoToMenu();
                    break;
            }
        }

        // ------------------------Funciones UI------------------------------
        private void MostrarPregunta()
        {
            _txtPregunta.text = "" + (_preguntaActual + 1) + ". " +
                                _respuestas[_preguntaActual].GetQuestion().GetQuestionText();
            _txtRespuestas[0].text = "a) " + _respuestas[_preguntaActual].GetQuestion().GetAnserTexts()[0];
            _txtRespuestas[1].text = "b) " + _respuestas[_preguntaActual].GetQuestion().GetAnserTexts()[1];
            _txtRespuestas[2].text = "c) " + _respuestas[_preguntaActual].GetQuestion().GetAnserTexts()[2];

            ColorearRespuestaSeleccionada();

            if (_preguntaActual <= 0)
            {
                _btnAtras.gameObject.SetActive(false);
                _btnAceptar.transform.localPosition = _btnAceptar.transform.localPosition + Vector3.left*216;
            }
            else
            {
                _btnAtras.gameObject.SetActive(true);
                _btnAtras.transform.GetChild(0).GetComponent<Text>().text = "ATRÁS";

                _btnAceptar.transform.localPosition = new Vector3(216, _btnAceptar.transform.localPosition.y,
                    _btnAceptar.transform.localPosition.z);
            }

            if (_preguntaActual >= _respuestas.Count - 1)
                _btnAceptar.transform.GetChild(0).GetComponent<Text>().text = "TERMINAR";
            else _btnAceptar.transform.GetChild(0).GetComponent<Text>().text = "ACEPTAR";
        }

        private void ColorearRespuestaSeleccionada()
        {
            for (var index = 0; index < _txtRespuestas.Count; index++)
                if (index == _respuestas[_preguntaActual].GetAnswer()) _txtRespuestas[index].color = _answerSettedColor;
                else _txtRespuestas[index].color = _defaultAnswerColor;
        }

        private void MostrarResultados()
        {
            _popUps[0].SetActive(false);
            _popUps[1].SetActive(true);

            var correctas = 0;
            _txtRespuesta.gameObject.SetActive(true);
            for (var index = 0; index < _respuestas.Count; index++)
            {
                var txtRespuestaClone = Instantiate(_txtRespuesta.gameObject);
                txtRespuestaClone.transform.SetParent(_txtRespuesta.transform.parent);
                txtRespuestaClone.transform.localScale = new Vector3(1, 1, 1);
                txtRespuestaClone.transform.localPosition = _txtRespuesta.transform.localPosition +
                                                            Vector3.down*index*50;
                txtRespuestaClone.GetComponent<Text>().text = index + 1 + ".";

                if (_respuestas[index].GetQuestion().ReviewQuestion(_respuestas[index].GetAnswer()))
                {
                    txtRespuestaClone.GetComponent<Text>().text += " Correcto \t\t2";
                    txtRespuestaClone.GetComponent<Text>().color = Color.green;
                    correctas++;
                }
                else
                {
                    txtRespuestaClone.GetComponent<Text>().text += " Incorrecto \t0";
                    txtRespuestaClone.GetComponent<Text>().color = Color.red;
                }
            }
            _txtRespuesta.gameObject.SetActive(false);
            _txtCalificacion.text = "Total:  \t\t" + correctas*2 + " / 10";
        }

        private void LimpiarResultados()
        {
            foreach (Transform child in _popUps[1].transform)
                if (child.name == "resultadoPregunta(Clone)") Destroy(child.gameObject);
        }

        // ------------------------Funciones Botones-------------------------

        private void Regresar()
        {
            _preguntaActual--;
            MostrarPregunta();
        }

        private void Avanzar()
        {
            if (_preguntaActual >= _respuestas.Count - 1)
            {
                StartCoroutine(SecuenciaEventos("evaluar", 0.1f));
            }
            else
            {
                _preguntaActual++;
                MostrarPregunta();
            }
        }

        private void SeleccionarRespuesta(int boton)
        {
            _respuestas[_preguntaActual].SetAnswer(boton);
            //sound answer
            PlaySoundAnswerSelected();
            ColorearRespuestaSeleccionada();
        }

        // ------------------------Funciones Evaluacion----------------------

        private void SeleccionarPreguntasRandom(int numeroPreguntas)
        {
            _respuestas = new List<Answer>();
            for (var i = 0; i < numeroPreguntas; i++)
            {
                var randomIndex = Random.Range(0, _preguntas.Count);
                _respuestas.Add(new Answer(_preguntas[randomIndex], -1));
                _preguntas.RemoveAt(randomIndex);
            }
        }

        private void CrearCuestionario()
        {
            _preguntas = new List<Question>();
            _preguntas.Add(
                new Question("Tengo 375 pesos y compro un juguete que cuesta 115 pesos, ¿cuánto dinero me queda?",
                    new[] {"$ 150", "$ 260", "$ 270"},
                    1
                ));

            _preguntas.Add(
                new Question(
                    "Quiero comprar una botella de agua que cuesta $7, una barra de pan que cuesta $35 y dos manzanas que cuestan $16. ¿Cuánto van a cobrarme?",
                    new[] {"$ 56", "$ 57", "$ 58"},
                    2
                ));
            _preguntas.Add(
                new Question(
                    "Para un proyecto en la escuela necesito 12 cartulinas, a 4 pesos cada una, ¿cuánto me cuestan las 12?",
                    new[] {"$ 18", "$ 38", "$ 48"},
                    2
                ));
            _preguntas.Add(
                new Question(
                    "Tengo 72 dulces, quiero repartirlos entre mis 8 invitados, ¿cuántos dulces les puedo dar a cada uno?",
                    new[] {"$ 4", "$ 9", "$ 12"},
                    1
                ));
            _preguntas.Add(
                new Question(
                    "Tengo 50 pesos, y compro un paquete de galletas que cuesta 7 pesos, ¿cuánto dinero me queda?",
                    new[] {"$ 42", "$ 43", "$ 44"},
                    1
                ));
            _preguntas.Add(
                new Question(
                    "Tengo 24 paletas, quiero repartirlas entre mis 3 amigos, ¿cuántas paletas les puedo dar a cada uno?",
                    new[] {"$ 4", "$ 6", "$ 8"},
                    2
                ));
            _preguntas.Add(
                new Question("Compré un kilo de tortilla en 14 pesos, mi mamá me dio 20 pesos, ¿qué puedo comprarme?",
                    new[] {"1 paquete de galletas a $ 7", "3 mazapanes a $2 cada uno", "Helado a $ 16"},
                    1
                ));
            _preguntas.Add(
                new Question(
                    "Mi tía me regaló 150 pesos para mi cumpleaños y tengo ahorrado 65 pesos, ¿cuánto dinero tengo en total?",
                    new[] {"$ 205", "$ 210", "$ 215"},
                    2
                ));
        }

        private void PlaySoundEvaluation()
        {
            _soundManager.PlaySound("Sound_evaluation", 1);
        }

        private void PlaySoundAnswerSelected()
        {
            _soundManager.PlaySound("Sound_answerSelected", 2);
        }
    }

    public class Question
    {
        private readonly string[] _answersTexts;
        private readonly int _correctAnswer;
        private readonly string _questionText;

        /// <summary>
        ///     Question with only one correct answer
        /// </summary>
        /// <param name="questionTest">Here comes the question string</param>
        /// <param name="answerTexts">Here comes all the text of each answer</param>
        /// <param name="correctAnswer">Correct Answer, NOTE it has to be int starting in 0</param>
        public Question(string questionTest, string[] answerTexts, int correctAnswer)
        {
            _questionText = questionTest;
            _answersTexts = answerTexts;
            _correctAnswer = correctAnswer;
        }

        public bool ReviewQuestion(int answer)
        {
            if (answer == _correctAnswer) return true;
            return false;
        }

        public string GetQuestionText()
        {
            return _questionText;
        }

        public string[] GetAnserTexts()
        {
            return _answersTexts;
        }
    }

    public class Answer
    {
        private readonly Question _question;
        private int _answer;

        /// <summary>
        ///     Answer for a single answer question
        /// </summary>
        /// <param name="question">Question object</param>
        /// <param name="answer">Answer to the question object</param>
        public Answer(Question question, int answer)
        {
            _question = question;
            _answer = answer;
        }

        public Question GetQuestion()
        {
            return _question;
        }

        public int GetAnswer()
        {
            return _answer;
        }

        public void SetAnswer(int answer)
        {
            _answer = answer;
        }
    }
}