﻿using UnityEngine;
using UnityEngine.UI;

namespace Assets.Resources.Scripts
{
    public class DropDownMenu : MonoBehaviour
    {
        private Animator _animMenu;
        private Button _btnClosePopUp;
        private Button _btnCreditos;
        private Button _btnFichaTecnica;
        private Button _btnGlosario;
        private Button _btnInicio;
        private Button _btnToggleMenu;
        private GameObject _goCreditos;
        private GameObject _goDropDownMenu;
        private GameObject _goFichaTecnica;
        private GameObject _goGlosario;
        private bool _isMenuOpen;
        private MenuOda12 _mainMenu;
        // Use this for initialization
        private void Start()
        {
            _mainMenu = transform.GetComponent<MenuOda12>();
            _goDropDownMenu = transform.FindChild("genericUI").Find("dropDownMenu").gameObject;
            _btnToggleMenu = _goDropDownMenu.transform.GetChild(0).GetComponent<Button>();
            _animMenu = _goDropDownMenu.transform.GetChild(0).GetChild(0).GetComponent<Animator>();
            _btnClosePopUp = _goDropDownMenu.transform.GetChild(1).GetComponent<Button>();
            _btnFichaTecnica = _animMenu.transform.Find("btnFichaTecnica").GetComponent<Button>();
            _btnCreditos = _animMenu.transform.Find("btnCreditos").GetComponent<Button>();
            _btnGlosario = _animMenu.transform.Find("btnGlosario").GetComponent<Button>();
            _btnInicio = _animMenu.transform.Find("btnInicio").GetComponent<Button>();
            _goFichaTecnica = _btnClosePopUp.transform.GetChild(0).GetChild(0).gameObject;
            _goCreditos = _btnClosePopUp.transform.GetChild(0).GetChild(1).gameObject;
            _goGlosario = _btnClosePopUp.transform.GetChild(0).GetChild(2).gameObject;

            _btnToggleMenu.onClick.AddListener(ToggleMenu);
            _btnClosePopUp.onClick.AddListener(ClosePopUp);
            _btnFichaTecnica.onClick.AddListener(delegate { OpenPopUp("FichaTecnica"); });
            _btnCreditos.onClick.AddListener(delegate { OpenPopUp("Creditos"); });
            _btnGlosario.onClick.AddListener(delegate { OpenPopUp("Glosario"); });
            _btnInicio.onClick.AddListener(GoToInicio);
        }

        private void ClosePopUp()
        {
            _goFichaTecnica.SetActive(false);
            _goCreditos.SetActive(false);
            _goGlosario.SetActive(false);
            _btnClosePopUp.gameObject.SetActive(false);
        }

        private void GoToInicio()
        {
            _mainMenu.GoToMenu();
            ToggleMenu();
        }

        private void OpenPopUp(string popUpName)
        {
            _btnClosePopUp.gameObject.SetActive(true);

            switch (popUpName)
            {
                case "FichaTecnica":
                    _goFichaTecnica.SetActive(true);
                    break;
                case "Creditos":
                    _goCreditos.SetActive(true);
                    break;
                case "Glosario":
                    _goGlosario.SetActive(true);
                    break;
            }
        }

        private void ToggleMenu()
        {
            _isMenuOpen = !_isMenuOpen;
            _animMenu.SetBool("Opened", _isMenuOpen);
        }

        // Update is called once per frame
        private void Update()
        {
            _btnInicio.gameObject.SetActive(_mainMenu.IsActiveChallenge());
        }
    }
}