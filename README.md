﻿[![CIMAT](https://s28.postimg.org/bvasd300t/image.png)](http://www.ingsoft.info/)  
  
# Readme ODA 12 - ¡Preparando la fiesta!  
Repositorio correspondiente a la ODA 12 - Preparando la fiesta.    
  
## Información general  
ODA desarrollada por: Luis Angel Hernández Lázaro  
Contacto:  
  - luis.hernadez@cimat.mx  
  - luishdezlazaro@hotmail.com  
  - Teléfono Celular 2282354184  
  
## Instalación  
Instrucciones para clonar el repositorio:  
  - Abrir una nueva terminal, en el caso de windows abrir una terminal desde GitBash (Descargar e instalar desde [aqui](https://git-for-windows.github.io/)).  
  - Crear un nuevo direcotrio para el proyecto:  
```sh  
$ mkdir oda12  
$ cd oda12  
```  
  - Ejecutar el siguiente comando:  
```sh  
$ git clone https://lvyzh@bitbucket.org/lvyzh/oda-12-preparando-la-fiesta.git  
```  
  - Abrir Unity3d y seleccionar la carpeta del proyecto:   
```sh  
/yourpath/oda12/oda-12-preparando-la-fiesta  
```  
  
## Estructura de los directorios  
A continuación se presenta la estrucutra de los directorios para navegar:  
  
### Directorio ODA 12 - ¡Preparando la fiesta! - Documentation  
Este directorio contiene los archivos relacionados a la documentación del desarrollo de la ODA, incluyendo:  
- El guión de la ODA  
- Las observaciones por parte de la empresa Chiva Sentada  
- Los archivos de Postmortem  
- El reporte final de la ODA.  
  
#### ODA 12 - ¡Preparando la fiesta! - Observations  
- Guion  
- Observaciones  
  
#### ODA 12 - ¡Preparando la fiesta! - Postmortem  
- Reporte de valor ganado y horas trabajadas  
  
#### ODA 12 - ¡Preparando la fiesta! - Report   
 - Reporte final de ODA  
  
### Directorio ODA 12 - ¡Preparando la fiesta! - ReleaseFiles - Unity3d - apk  
Este directorio contiene los archivos relacionados con la liberación de la ODA, incluye los archivos:  
 - oda12.unity3d  
 - oda12.apk  
  
### Directorio ODA 12 - ¡Preparando la fiesta! - with Script Windows  
Este directorio contienen los archivos relacionados con el desarrollo de la ODA, contiene los scripts vinculados con el proyecto en unity para realizar el desarrollo y las pruebas de manera local.  
  
### Directorio ODA 12 - ¡Preparando la fiesta! - without Script MAC  
Este directorio contiene los archivos relacionas con el desarrollo de la ODA, contiene el proyecto desde Unity3d "SIN" vincular los archivos de los scripts. Este directorio es usado para el desarrollo de los archivos .bytes y .unity3d  
  
## Árbol de directorios  
+-- ODA 12 - ¡Preparando la fiesta! - Documentation  
¦   +-- ODA 12 - ¡Preparando la fiesta! - Observations  
¦   ¦   +-- ODA 12 - ¡Preparando la fiesta! - Guion.docx  
¦   ¦   +-- ODA 12 - ¡Preparando la fiesta! - Observations.xlsx  
¦   +-- ODA 12 - ¡Preparando la fiesta! - Postmortem  
¦   ¦   +-- 10-ODA 12 - ¡Preparando la fiesta! - Postmortem Evaluación.docx  
¦   ¦   +-- 1-ODA 12 - ¡Preparando la fiesta! - Postmortem Entorno.docx  
¦   ¦   +-- 2-ODA 12 - ¡Preparando la fiesta! - Postmortem Botones.docx  
¦   ¦   +-- 3-ODA 12 - ¡Preparando la fiesta! - Postmortem Avanzar.docx  
¦   ¦   +-- 4-ODA 12 - ¡Preparando la fiesta! - Postmortem PopUp.docx  
¦   ¦   +-- 5-ODA 12 - ¡Preparando la fiesta! - Postmortem Manipular.docx  
¦   ¦   +-- 6-ODA 12 - ¡Preparando la fiesta! - Postmortem Sonidos.docx  
¦   ¦   +-- 7-ODA 12 - ¡Preparando la fiesta! - Postmortem Puntaje.docx  
¦   ¦   +-- 8-ODA 12 - ¡Preparando la fiesta! - Postmortem Parpadeo.docx  
¦   ¦   +-- 9-ODA 12 - ¡Preparando la fiesta! - Postmortem AlmacenarRecord.docx  
¦   +-- ODA 12 - ¡Preparando la fiesta! - Report  
¦       +-- ODA 12 - ¡Preparando la fiesta! - Report.docx  
¦       +-- ODA 12 - ¡Preparando la fiesta! - ScreenShots  
¦           +-- Screenshot_2016-12-19-15-27-50.png  
¦           +-- Screenshot_2016-12-19-16-12-13.png  
¦           +-- Screenshot_2016-12-19-16-12-26.png  
¦           +-- Screenshot_2016-12-19-16-12-29.png  
¦           +-- Screenshot_2016-12-19-16-12-36.png  
¦           +-- Screenshot_2016-12-19-16-12-51.png  
¦           +-- Screenshot_2016-12-19-16-12-56.png  
¦           +-- Screenshot_2016-12-19-16-12-59.png  
¦           +-- Screenshot_2016-12-19-16-13-07.png  
¦           +-- Screenshot_2016-12-19-16-13-11.png  
¦           +-- Screenshot_2016-12-19-16-13-15.png  
¦           +-- Screenshot_2016-12-19-16-13-19.png  
¦           +-- Screenshot_2016-12-19-16-13-26.png  
¦           +-- Screenshot_2016-12-19-16-13-32.png  
¦           +-- Screenshot_2016-12-19-16-13-36.png  
¦           +-- Screenshot_2016-12-19-16-13-41.png  
¦           +-- Screenshot_2016-12-19-16-14-09.png  
¦           +-- Screenshot_2016-12-19-16-14-13.png  
¦           +-- Screenshot_2016-12-19-16-14-19.png  
¦           +-- Screenshot_2016-12-19-16-14-22.png  
¦           +-- Screenshot_2016-12-19-16-14-26.png  
¦           +-- Screenshot_2016-12-19-16-14-29.png  
¦           +-- Screenshot_2016-12-19-16-14-33.png  
¦           +-- Screenshot_2016-12-19-16-14-40.png  
¦           +-- Screenshot_2016-12-19-16-14-43.png  
¦           +-- Screenshot_2016-12-19-16-14-48.png  
¦           +-- Screenshot_2016-12-19-16-14-56.png  
¦           +-- Screenshot_2016-12-19-16-15-00.png  
¦           +-- Screenshot_2016-12-19-16-15-02.png  
¦           +-- Screenshot_2016-12-19-16-15-06.png  
¦           +-- Screenshot_2016-12-19-16-15-09.png  
¦           +-- Screenshot_2016-12-19-16-15-15.png  
¦           +-- Screenshot_2016-12-19-16-15-19.png  
¦           +-- Screenshot_2016-12-19-16-15-23.png  
¦           +-- Screenshot_2016-12-19-16-15-27.png  
¦           +-- Screenshot_2016-12-19-16-17-35.png  
¦           +-- Screenshot_2016-12-19-16-17-38.png  
¦           +-- Screenshot_2016-12-19-16-17-41.png  
¦           +-- Screenshot_2016-12-19-16-17-45.png  
¦           +-- Screenshot_2016-12-19-16-17-48.png  
¦           +-- Screenshot_2016-12-19-16-17-51.png  
¦           +-- Screenshot_2016-12-19-16-17-54.png  
¦           +-- Screenshot_2016-12-19-16-17-58.png  
¦           +-- Screenshot_2016-12-19-16-18-02.png  
¦           +-- Screenshot_2016-12-19-16-18-06.png  
¦           +-- Screenshot_2016-12-19-16-18-11.png  
¦           +-- Screenshot_2016-12-19-16-18-14.png  
¦           +-- Screenshot_2016-12-19-16-18-17.png  
¦           +-- Screenshot_2016-12-19-16-18-21.png  
¦           +-- Screenshot_2016-12-19-16-18-24.png  
¦           +-- Screenshot_2016-12-19-16-18-29.png  
¦           +-- Screenshot_2016-12-19-16-18-33.png  
¦           +-- Screenshot_2016-12-19-16-18-37.png  
¦           +-- Screenshot_2016-12-19-16-18-39.png  
¦           +-- Screenshot_2016-12-19-16-18-45.png  
¦           +-- Screenshot_2016-12-19-16-18-49.png  
¦           +-- Screenshot_2016-12-19-16-18-52.png  
¦           +-- Screenshot_2016-12-19-16-18-56.png  
¦           +-- Screenshot_2016-12-19-16-19-01.png  
¦           +-- Screenshot_2016-12-19-16-19-04.png  
¦           +-- Screenshot_2016-12-19-16-19-22.png  
¦           +-- Screenshot_2016-12-19-16-19-29.png  
¦           +-- Screenshot_2016-12-19-16-19-34.png  
+-- ODA 12 - ¡Preparando la fiesta! - ReleaseFiles - Unity3d - apk  
¦   +-- ODA12.apk  
¦   +-- ODA12.unity3d  
+-- ODA 12 - ¡Preparando la fiesta! - without Script MAC  
¦   +-- Assets  
¦   ¦   +-- Resources  
¦   ¦   ¦   +-- Challenge1  
¦   ¦   ¦   ¦   +-- Background.png  
¦   ¦   ¦   ¦   +-- Label.png  
¦   ¦   ¦   ¦   +-- Snake.png  
¦   ¦   ¦   ¦   +-- Teddies  
¦   ¦   ¦   ¦   ¦   +-- 107.png  
¦   ¦   ¦   ¦   ¦   +-- 111.png  
¦   ¦   ¦   ¦   ¦   +-- 122.png  
¦   ¦   ¦   ¦   ¦   +-- 133.png  
¦   ¦   ¦   ¦   ¦   +-- 139.png  
¦   ¦   ¦   ¦   ¦   +-- 149.png  
¦   ¦   ¦   ¦   ¦   +-- 150.png  
¦   ¦   ¦   ¦   ¦   +-- 167.png  
¦   ¦   ¦   ¦   ¦   +-- 249.png  
¦   ¦   ¦   ¦   ¦   +-- 279.png  
¦   ¦   ¦   ¦   ¦   +-- 79.png  
¦   ¦   ¦   ¦   ¦   +-- 98.png  
¦   ¦   ¦   ¦   ¦   +-- Teddy1-bri.png  
¦   ¦   ¦   ¦   ¦   +-- Teddy1.png  
¦   ¦   ¦   ¦   ¦   +-- Teddy2-bri.png  
¦   ¦   ¦   ¦   ¦   +-- Teddy2.png  
¦   ¦   ¦   ¦   ¦   +-- Teddy3-bri.png  
¦   ¦   ¦   ¦   ¦   +-- Teddy3.png  
¦   ¦   ¦   ¦   ¦   +-- Teddy4-bri.png  
¦   ¦   ¦   ¦   ¦   +-- Teddy4.png  
¦   ¦   ¦   ¦   +-- Toys  
¦   ¦   ¦   ¦       +-- AirPlane-bri.png  
¦   ¦   ¦   ¦       +-- AirPlane.png  
¦   ¦   ¦   ¦       +-- AirPlanePrice.png  
¦   ¦   ¦   ¦       +-- Book-bri.png  
¦   ¦   ¦   ¦       +-- Book.png  
¦   ¦   ¦   ¦       +-- BookPrice.png  
¦   ¦   ¦   ¦       +-- Car-bri.png  
¦   ¦   ¦   ¦       +-- CarPrice.png  
¦   ¦   ¦   ¦       +-- DollHouse-bri.png  
¦   ¦   ¦   ¦       +-- DollHouse.png  
¦   ¦   ¦   ¦       +-- DollHousePrice.png  
¦   ¦   ¦   ¦       +-- Movie-bri.png  
¦   ¦   ¦   ¦       +-- Movie.png  
¦   ¦   ¦   ¦       +-- MoviePrice.png  
¦   ¦   ¦   ¦       +-- Piano-bri.png  
¦   ¦   ¦   ¦       +-- Piano.png  
¦   ¦   ¦   ¦       +-- PianoPrice.png  
¦   ¦   ¦   ¦       +-- Robot-bri.png  
¦   ¦   ¦   ¦       +-- Robot.png  
¦   ¦   ¦   ¦       +-- RobotPrice.png  
¦   ¦   ¦   ¦       +-- SoccerBall-Bri.png  
¦   ¦   ¦   ¦       +-- SoccerBall.png  
¦   ¦   ¦   ¦       +-- SoccerBallPrice.png  
¦   ¦   ¦   ¦       +-- VideoGame-bri.png  
¦   ¦   ¦   ¦       +-- VideoGame.png  
¦   ¦   ¦   ¦       +-- VideoGamePrice.png  
¦   ¦   ¦   +-- Challenge2  
¦   ¦   ¦   ¦   +-- 100.png  
¦   ¦   ¦   ¦   +-- 120.png  
¦   ¦   ¦   ¦   +-- 16.png  
¦   ¦   ¦   ¦   +-- 4.png  
¦   ¦   ¦   ¦   +-- 80.png  
¦   ¦   ¦   ¦   +-- 8.png  
¦   ¦   ¦   ¦   +-- Activity2  
¦   ¦   ¦   ¦   ¦   +-- Background.png  
¦   ¦   ¦   ¦   ¦   +-- Chocolate.png  
¦   ¦   ¦   ¦   ¦   +-- GenericLabel.png  
¦   ¦   ¦   ¦   ¦   +-- Mazapan.png  
¦   ¦   ¦   ¦   ¦   +-- Package1.png  
¦   ¦   ¦   ¦   ¦   +-- Package2.png  
¦   ¦   ¦   ¦   ¦   +-- Package3.jpg  
¦   ¦   ¦   ¦   ¦   +-- Package4.jpg  
¦   ¦   ¦   ¦   ¦   +-- Palette.png  
¦   ¦   ¦   ¦   ¦   +-- Tamarind.png  
¦   ¦   ¦   ¦   +-- Background.jpg  
¦   ¦   ¦   ¦   +-- PinataHorse.png  
¦   ¦   ¦   ¦   +-- PinataSpecial.png  
¦   ¦   ¦   ¦   +-- PinataTraditional.png  
¦   ¦   ¦   +-- Challenge3  
¦   ¦   ¦   ¦   +-- Animations  
¦   ¦   ¦   ¦   ¦   +-- Candies 1.controller  
¦   ¦   ¦   ¦   ¦   +-- Candies 2.controller  
¦   ¦   ¦   ¦   ¦   +-- Candies 3.controller  
¦   ¦   ¦   ¦   ¦   +-- Candies 4.controller  
¦   ¦   ¦   ¦   ¦   +-- Candies 5.controller  
¦   ¦   ¦   ¦   ¦   +-- Candies 6.controller  
¦   ¦   ¦   ¦   ¦   +-- Candies 7.controller  
¦   ¦   ¦   ¦   ¦   +-- Candies 8.controller  
¦   ¦   ¦   ¦   ¦   +-- CandiesBack.controller  
¦   ¦   ¦   ¦   ¦   +-- Candies.controller  
¦   ¦   ¦   ¦   ¦   +-- MovementsA1.anim  
¦   ¦   ¦   ¦   ¦   +-- MovementsA1back.anim  
¦   ¦   ¦   ¦   ¦   +-- MovementsA2.anim  
¦   ¦   ¦   ¦   ¦   +-- MovementsA3.anim  
¦   ¦   ¦   ¦   ¦   +-- MovementsB1.anim  
¦   ¦   ¦   ¦   ¦   +-- MovementsB2.anim  
¦   ¦   ¦   ¦   ¦   +-- MovementsB3.anim  
¦   ¦   ¦   ¦   ¦   +-- MovementsC1.anim  
¦   ¦   ¦   ¦   ¦   +-- MovementsC2.anim  
¦   ¦   ¦   ¦   ¦   +-- MovementsC3.anim  
¦   ¦   ¦   ¦   ¦   +-- OptionB3.controller  
¦   ¦   ¦   ¦   +-- Background.jpg  
¦   ¦   ¦   ¦   +-- candies  
¦   ¦   ¦   ¦       +-- bolo1.png  
¦   ¦   ¦   ¦       +-- bolo2.png  
¦   ¦   ¦   ¦       +-- Copia de CHOCOLATE.png  
¦   ¦   ¦   ¦       +-- Copia de MAZAPAN.png  
¦   ¦   ¦   ¦       +-- Copia de PALETA.png  
¦   ¦   ¦   ¦       +-- Copia de TAMARINDO.png  
¦   ¦   ¦   +-- Challenge4  
¦   ¦   ¦   ¦   +-- Animations  
¦   ¦   ¦   ¦   ¦   +-- AnimationCandies1.anim  
¦   ¦   ¦   ¦   ¦   +-- AnimationCandies.anim  
¦   ¦   ¦   ¦   ¦   +-- AnimationPinataHorseBom.anim  
¦   ¦   ¦   ¦   ¦   +-- AnimationPinataSpecialBom.anim  
¦   ¦   ¦   ¦   ¦   +-- AnimationPinataTraditionalBom.anim  
¦   ¦   ¦   ¦   ¦   +-- AnimationStick1.anim  
¦   ¦   ¦   ¦   ¦   +-- AnimationStick2.anim  
¦   ¦   ¦   ¦   ¦   +-- AnimationStick3.anim  
¦   ¦   ¦   ¦   ¦   +-- Candies.controller  
¦   ¦   ¦   ¦   ¦   +-- RetoFinal.prefab  
¦   ¦   ¦   ¦   ¦   +-- Stick1.controller  
¦   ¦   ¦   ¦   ¦   +-- Stick2.controller  
¦   ¦   ¦   ¦   ¦   +-- Stick3.controller  
¦   ¦   ¦   ¦   +-- Pinatas  
¦   ¦   ¦   ¦   ¦   +-- pinataHorse  
¦   ¦   ¦   ¦   ¦   ¦   +-- AnimationHorse.anim  
¦   ¦   ¦   ¦   ¦   ¦   +-- AnimationPinataHorseBom.anim  
¦   ¦   ¦   ¦   ¦   ¦   +-- p1.png  
¦   ¦   ¦   ¦   ¦   ¦   +-- p2.png  
¦   ¦   ¦   ¦   ¦   ¦   +-- p3.png  
¦   ¦   ¦   ¦   ¦   ¦   +-- p4.png  
¦   ¦   ¦   ¦   ¦   ¦   +-- p5.png  
¦   ¦   ¦   ¦   ¦   ¦   +-- p6.png  
¦   ¦   ¦   ¦   ¦   ¦   +-- p7.png  
¦   ¦   ¦   ¦   ¦   ¦   +-- p8.png  
¦   ¦   ¦   ¦   ¦   ¦   +-- piñata-caballo _P_.png  
¦   ¦   ¦   ¦   ¦   ¦   +-- PinataHorse.controller  
¦   ¦   ¦   ¦   ¦   +-- pinataSpecial  
¦   ¦   ¦   ¦   ¦   ¦   +-- a1.png  
¦   ¦   ¦   ¦   ¦   ¦   +-- a2.png  
¦   ¦   ¦   ¦   ¦   ¦   +-- a3.png  
¦   ¦   ¦   ¦   ¦   ¦   +-- a4.png  
¦   ¦   ¦   ¦   ¦   ¦   +-- a5.png  
¦   ¦   ¦   ¦   ¦   ¦   +-- a6.png  
¦   ¦   ¦   ¦   ¦   ¦   +-- a7.png  
¦   ¦   ¦   ¦   ¦   ¦   +-- AnimationPinataSpecialBom.anim  
¦   ¦   ¦   ¦   ¦   ¦   +-- AnimationSpecial.anim  
¦   ¦   ¦   ¦   ¦   ¦   +-- piñata-especial _a_.png  
¦   ¦   ¦   ¦   ¦   ¦   +-- PinataSpecial.controller  
¦   ¦   ¦   ¦   ¦   +-- pinataTraditional  
¦   ¦   ¦   ¦   ¦       +-- AnimationPinataTraditionalBom.anim  
¦   ¦   ¦   ¦   ¦       +-- AnimationTraditional.anim  
¦   ¦   ¦   ¦   ¦       +-- f1.png  
¦   ¦   ¦   ¦   ¦       +-- f2.png  
¦   ¦   ¦   ¦   ¦       +-- f3.png  
¦   ¦   ¦   ¦   ¦       +-- f4.png  
¦   ¦   ¦   ¦   ¦       +-- f5.png  
¦   ¦   ¦   ¦   ¦       +-- PinataTraditional.controller  
¦   ¦   ¦   ¦   +-- StarAreaStick.png  
¦   ¦   ¦   ¦   +-- Stick1.png  
¦   ¦   ¦   ¦   +-- Stick2.png  
¦   ¦   ¦   ¦   +-- Stick3.png  
¦   ¦   ¦   +-- ExportAssetsBundle2.cs  
¦   ¦   ¦   +-- FinalChallenge  
¦   ¦   ¦   ¦   +-- Background.jpg  
¦   ¦   ¦   +-- Generics  
¦   ¦   ¦   ¦   +-- background.png  
¦   ¦   ¦   ¦   +-- botónMenu.png  
¦   ¦   ¦   ¦   +-- BtnBoton.png  
¦   ¦   ¦   ¦   +-- Globe.png  
¦   ¦   ¦   ¦   +-- GrandMother.png  
¦   ¦   ¦   ¦   +-- PopUp.png  
¦   ¦   ¦   ¦   +-- Quenda-Medium.otf  
¦   ¦   ¦   ¦   +-- scores  
¦   ¦   ¦   ¦       +-- starArea.png  
¦   ¦   ¦   ¦       +-- star.png  
¦   ¦   ¦   +-- InitScreen  
¦   ¦   ¦   ¦   +-- background.jpg  
¦   ¦   ¦   +-- MainMenu  
¦   ¦   ¦   ¦   +-- Animations  
¦   ¦   ¦   ¦   ¦   +-- AnimationChallenge1.anim  
¦   ¦   ¦   ¦   ¦   +-- AnimationChallenge2.anim  
¦   ¦   ¦   ¦   ¦   +-- AnimationChallenge3.anim  
¦   ¦   ¦   ¦   ¦   +-- AnimationChallenge4.anim  
¦   ¦   ¦   ¦   ¦   +-- AnimationChallengeFinal.anim  
¦   ¦   ¦   ¦   ¦   +-- BtnChallenge1.controller  
¦   ¦   ¦   ¦   ¦   +-- BtnChallenge2.controller  
¦   ¦   ¦   ¦   ¦   +-- BtnChallenge3.controller  
¦   ¦   ¦   ¦   ¦   +-- BtnChallenge4.controller  
¦   ¦   ¦   ¦   ¦   +-- BtnFinalChallenge.controller  
¦   ¦   ¦   ¦   ¦   +-- genericUI.controller  
¦   ¦   ¦   ¦   +-- Background.jpg  
¦   ¦   ¦   ¦   +-- Challenge1.png  
¦   ¦   ¦   ¦   +-- Challenge2.png  
¦   ¦   ¦   ¦   +-- Challenge3.png  
¦   ¦   ¦   ¦   +-- FinalChallenge.png  
¦   ¦   ¦   ¦   +-- genericUI  
¦   ¦   ¦   ¦   ¦   +-- DropDownMenu  
¦   ¦   ¦   ¦   ¦   ¦   +-- Credits.png  
¦   ¦   ¦   ¦   ¦   ¦   +-- DataSheet.png  
¦   ¦   ¦   ¦   ¦   ¦   +-- DropDownMenuAnimation.anim  
¦   ¦   ¦   ¦   ¦   ¦   +-- dropDownMenu.prefab  
¦   ¦   ¦   ¦   ¦   ¦   +-- glosario08.png  
¦   ¦   ¦   ¦   ¦   ¦   +-- MenuClose.anim  
¦   ¦   ¦   ¦   ¦   ¦   +-- menuContainer.controller  
¦   ¦   ¦   ¦   ¦   ¦   +-- MenuOpen.anim  
¦   ¦   ¦   ¦   ¦   ¦   +-- New Animation213213.anim  
¦   ¦   ¦   ¦   ¦   +-- genericUI.prefab  
¦   ¦   ¦   ¦   +-- reto4.png  
¦   ¦   ¦   +-- oda12.bytes  
¦   ¦   ¦   +-- ODA12.prefab  
¦   ¦   ¦   +-- Scripts  
¦   ¦   ¦       +-- DropDownMenu.cs  
¦   ¦   ¦       +-- InitOda12.cs  
¦   ¦   ¦       +-- MenuOda12.cs  
¦   ¦   ¦       +-- Reto1Oda12.cs  
¦   ¦   ¦       +-- Reto2Oda12.cs  
¦   ¦   ¦       +-- Reto3Oda12.cs  
¦   ¦   ¦       +-- Reto4Oda12.cs  
¦   ¦   ¦       +-- RetoFinalOda12.cs  
¦   ¦   ¦       +-- SoundManager.cs  
¦   ¦   +-- Scenes  
¦   ¦   ¦   +-- ODA12.unity  
¦   ¦   +-- StreamingAssets  
¦   ¦       +-- ActividadesRolesFases-TSP.xlsx  
¦   ¦       +-- Sound_answerSelected.mp3  
¦   ¦       +-- Sound_closePopUp.mp3  
¦   ¦       +-- Sound_crashAndBoing.mp3  
¦   ¦       +-- Sound_endChallenge.mp3  
¦   ¦       +-- Sound_error.mp3  
¦   ¦       +-- Sound_evaluation.mp3  
¦   ¦       +-- Sound_Fondoevaluacion.mp3  
¦   ¦       +-- Sound_openPopUp.mp3  
¦   ¦       +-- Sound_pinata.mp3  
¦   ¦       +-- Sound_SeleccionarRespuesta.mp3  
¦   ¦       +-- Sound_success.mp3  
¦   ¦       +-- Tropical_Jazz_Paradise.mp3  
¦   +-- Library  
¦   ¦   +-- AnnotationManager  
¦   ¦   +-- assetDatabase3  
¦   ¦   +-- AssetImportState  
¦   ¦   +-- AssetServerCacheV3  
¦   ¦   +-- AssetVersioning.db  
¦   ¦   +-- BuildPlayer.prefs  
¦   ¦   +-- BuildSettings.asset  
¦   ¦   +-- CurrentLayout.dwlt  
¦   ¦   +-- CurrentMaximizeLayout.dwlt  
¦   ¦   +-- EditorUserBuildSettings.asset  
¦   ¦   +-- EditorUserSettings.asset  
¦   ¦   +-- expandedItems  
¦   ¦   +-- InspectorExpandedItems.asset  
¦   ¦   +-- LastSceneManagerSetup.txt  
¦   ¦   +-- LibraryFormatVersion.txt  
¦   ¦   +-- metadata  
¦   ¦   +-- MonoManager.asset  
¦   ¦   +-- ProjectSettings.asset  
¦   ¦   +-- ScriptAssemblies  
¦   ¦   ¦   +-- Assembly-CSharp.dll  
¦   ¦   ¦   +-- Assembly-CSharp.dll.mdb  
¦   ¦   ¦   +-- BuiltinAssemblies.stamp  
¦   ¦   +-- ScriptMapper  
¦   ¦   +-- ShaderCache  
¦   ¦   +-- ShaderCache.db  
¦   ¦   +-- UnityAssemblies  
¦   ¦       +-- Mono.Cecil.dll  
¦   ¦       +-- Mono.Cecil.xml  
¦   ¦       +-- UnityEditor.dll  
¦   ¦       +-- UnityEditor.iOS.Extensions.Common.dll  
¦   ¦       +-- UnityEditor.iOS.Extensions.Common.xml  
¦   ¦       +-- UnityEditor.iOS.Extensions.Xcode.dll  
¦   ¦       +-- UnityEditor.iOS.Extensions.Xcode.xml  
¦   ¦       +-- UnityEditor.xml  
¦   ¦       +-- UnityEngine.dll  
¦   ¦       +-- UnityEngine.Networking.dll  
¦   ¦       +-- UnityEngine.Networking.xml  
¦   ¦       +-- UnityEngine.UI.dll  
¦   ¦       +-- UnityEngine.UI.xml  
¦   ¦       +-- UnityEngine.xml  
¦   ¦       +-- version.txt  
¦   +-- oda12.android.unity3d.android.unity3d  
¦   +-- oda12.bytes  
¦   ¦   +-- oda12  
¦   ¦       +-- oda12  
¦   ¦           +-- DropDownMenu.cs  
¦   ¦           +-- InitOda12.cs  
¦   ¦           +-- MenuOda12.cs  
¦   ¦           +-- Properties  
¦   ¦           ¦   +-- AssemblyInfo.cs  
¦   ¦           +-- Reto1Oda12.cs  
¦   ¦           +-- Reto2Oda12.cs  
¦   ¦           +-- Reto3Oda12.cs  
¦   ¦           +-- Reto4Oda12.cs  
¦   ¦           +-- RetoFinalOda12.cs  
¦   ¦           +-- SoundManager.cs  
¦   +-- oda12.unity3d  
¦   +-- ProjectSettings  
¦       +-- AudioManager.asset  
¦       +-- ClusterInputManager.asset  
¦       +-- DynamicsManager.asset  
¦       +-- EditorBuildSettings.asset  
¦       +-- EditorSettings.asset  
¦       +-- GraphicsSettings.asset  
¦       +-- InputManager.asset  
¦       +-- NavMeshAreas.asset  
¦       +-- NetworkManager.asset  
¦       +-- Physics2DSettings.asset  
¦       +-- ProjectSettings.asset  
¦       +-- ProjectVersion.txt  
¦       +-- QualitySettings.asset  
¦       +-- TagManager.asset  
¦       +-- TimeManager.asset  
¦       +-- UnityAdsSettings.asset  
¦       +-- UnityConnectSettings.asset  
+-- ODA 12 - ¡Preparando la fiesta! - with Script Windows  
¦   +-- Assets  
¦   ¦   +-- Resources  
¦   ¦   ¦   +-- Challenge1  
¦   ¦   ¦   ¦   +-- Background.png  
¦   ¦   ¦   ¦   +-- Label.png  
¦   ¦   ¦   ¦   +-- Snake.png  
¦   ¦   ¦   ¦   +-- Teddies  
¦   ¦   ¦   ¦   ¦   +-- 107.png  
¦   ¦   ¦   ¦   ¦   +-- 111.png  
¦   ¦   ¦   ¦   ¦   +-- 122.png  
¦   ¦   ¦   ¦   ¦   +-- 133.png  
¦   ¦   ¦   ¦   ¦   +-- 139.png  
¦   ¦   ¦   ¦   ¦   +-- 149.png  
¦   ¦   ¦   ¦   ¦   +-- 150.png  
¦   ¦   ¦   ¦   ¦   +-- 167.png  
¦   ¦   ¦   ¦   ¦   +-- 249.png  
¦   ¦   ¦   ¦   ¦   +-- 279.png  
¦   ¦   ¦   ¦   ¦   +-- 79.png  
¦   ¦   ¦   ¦   ¦   +-- 98.png  
¦   ¦   ¦   ¦   ¦   +-- Teddy1-bri.png  
¦   ¦   ¦   ¦   ¦   +-- Teddy1.png  
¦   ¦   ¦   ¦   ¦   +-- Teddy2-bri.png  
¦   ¦   ¦   ¦   ¦   +-- Teddy2.png  
¦   ¦   ¦   ¦   ¦   +-- Teddy3-bri.png  
¦   ¦   ¦   ¦   ¦   +-- Teddy3.png  
¦   ¦   ¦   ¦   ¦   +-- Teddy4-bri.png  
¦   ¦   ¦   ¦   ¦   +-- Teddy4.png  
¦   ¦   ¦   ¦   +-- Toys  
¦   ¦   ¦   ¦       +-- AirPlane-bri.png  
¦   ¦   ¦   ¦       +-- AirPlane.png  
¦   ¦   ¦   ¦       +-- AirPlanePrice.png  
¦   ¦   ¦   ¦       +-- Book-bri.png  
¦   ¦   ¦   ¦       +-- Book.png  
¦   ¦   ¦   ¦       +-- BookPrice.png  
¦   ¦   ¦   ¦       +-- Car-bri.png  
¦   ¦   ¦   ¦       +-- CarPrice.png  
¦   ¦   ¦   ¦       +-- DollHouse-bri.png  
¦   ¦   ¦   ¦       +-- DollHouse.png  
¦   ¦   ¦   ¦       +-- DollHousePrice.png  
¦   ¦   ¦   ¦       +-- Movie-bri.png  
¦   ¦   ¦   ¦       +-- Movie.png  
¦   ¦   ¦   ¦       +-- MoviePrice.png  
¦   ¦   ¦   ¦       +-- Piano-bri.png  
¦   ¦   ¦   ¦       +-- Piano.png  
¦   ¦   ¦   ¦       +-- PianoPrice.png  
¦   ¦   ¦   ¦       +-- Robot-bri.png  
¦   ¦   ¦   ¦       +-- Robot.png  
¦   ¦   ¦   ¦       +-- RobotPrice.png  
¦   ¦   ¦   ¦       +-- SoccerBall-Bri.png  
¦   ¦   ¦   ¦       +-- SoccerBall.png  
¦   ¦   ¦   ¦       +-- SoccerBallPrice.png  
¦   ¦   ¦   ¦       +-- VideoGame-bri.png  
¦   ¦   ¦   ¦       +-- VideoGame.png  
¦   ¦   ¦   ¦       +-- VideoGamePrice.png  
¦   ¦   ¦   +-- Challenge2  
¦   ¦   ¦   ¦   +-- 100.png  
¦   ¦   ¦   ¦   +-- 120.png  
¦   ¦   ¦   ¦   +-- 16.png  
¦   ¦   ¦   ¦   +-- 4.png  
¦   ¦   ¦   ¦   +-- 80.png  
¦   ¦   ¦   ¦   +-- 8.png  
¦   ¦   ¦   ¦   +-- Activity2  
¦   ¦   ¦   ¦   ¦   +-- Background.png  
¦   ¦   ¦   ¦   ¦   +-- Chocolate.png  
¦   ¦   ¦   ¦   ¦   +-- GenericLabel.png  
¦   ¦   ¦   ¦   ¦   +-- Mazapan.png  
¦   ¦   ¦   ¦   ¦   +-- Package1.png  
¦   ¦   ¦   ¦   ¦   +-- Package2.png  
¦   ¦   ¦   ¦   ¦   +-- Package3.jpg  
¦   ¦   ¦   ¦   ¦   +-- Package4.jpg  
¦   ¦   ¦   ¦   ¦   +-- Palette.png  
¦   ¦   ¦   ¦   ¦   +-- Tamarind.png  
¦   ¦   ¦   ¦   +-- Background.jpg  
¦   ¦   ¦   ¦   +-- PinataHorse.png  
¦   ¦   ¦   ¦   +-- PinataSpecial.png  
¦   ¦   ¦   ¦   +-- PinataTraditional.png  
¦   ¦   ¦   +-- Challenge3  
¦   ¦   ¦   ¦   +-- Animations  
¦   ¦   ¦   ¦   ¦   +-- Candies 1.controller  
¦   ¦   ¦   ¦   ¦   +-- Candies 2.controller  
¦   ¦   ¦   ¦   ¦   +-- Candies 3.controller  
¦   ¦   ¦   ¦   ¦   +-- Candies 4.controller  
¦   ¦   ¦   ¦   ¦   +-- Candies 5.controller  
¦   ¦   ¦   ¦   ¦   +-- Candies 6.controller  
¦   ¦   ¦   ¦   ¦   +-- Candies 7.controller  
¦   ¦   ¦   ¦   ¦   +-- Candies 8.controller  
¦   ¦   ¦   ¦   ¦   +-- CandiesBack.controller  
¦   ¦   ¦   ¦   ¦   +-- Candies.controller  
¦   ¦   ¦   ¦   ¦   +-- MovementsA1.anim  
¦   ¦   ¦   ¦   ¦   +-- MovementsA1back.anim  
¦   ¦   ¦   ¦   ¦   +-- MovementsA2.anim  
¦   ¦   ¦   ¦   ¦   +-- MovementsA3.anim  
¦   ¦   ¦   ¦   ¦   +-- MovementsB1.anim  
¦   ¦   ¦   ¦   ¦   +-- MovementsB2.anim  
¦   ¦   ¦   ¦   ¦   +-- MovementsB3.anim  
¦   ¦   ¦   ¦   ¦   +-- MovementsC1.anim  
¦   ¦   ¦   ¦   ¦   +-- MovementsC2.anim  
¦   ¦   ¦   ¦   ¦   +-- MovementsC3.anim  
¦   ¦   ¦   ¦   ¦   +-- OptionB3.controller  
¦   ¦   ¦   ¦   +-- Background.jpg  
¦   ¦   ¦   ¦   +-- candies  
¦   ¦   ¦   ¦       +-- bolo1.png  
¦   ¦   ¦   ¦       +-- bolo2.png  
¦   ¦   ¦   ¦       +-- Copia de CHOCOLATE.png  
¦   ¦   ¦   ¦       +-- Copia de MAZAPAN.png  
¦   ¦   ¦   ¦       +-- Copia de PALETA.png  
¦   ¦   ¦   ¦       +-- Copia de TAMARINDO.png  
¦   ¦   ¦   +-- Challenge4  
¦   ¦   ¦   ¦   +-- Animations  
¦   ¦   ¦   ¦   ¦   +-- AnimationCandies1.anim  
¦   ¦   ¦   ¦   ¦   +-- AnimationCandies.anim  
¦   ¦   ¦   ¦   ¦   +-- AnimationPinataHorseBom.anim  
¦   ¦   ¦   ¦   ¦   +-- AnimationPinataSpecialBom.anim  
¦   ¦   ¦   ¦   ¦   +-- AnimationPinataTraditionalBom.anim  
¦   ¦   ¦   ¦   ¦   +-- AnimationStick1.anim  
¦   ¦   ¦   ¦   ¦   +-- AnimationStick2.anim  
¦   ¦   ¦   ¦   ¦   +-- AnimationStick3.anim  
¦   ¦   ¦   ¦   ¦   +-- Candies.controller  
¦   ¦   ¦   ¦   ¦   +-- RetoFinal.prefab  
¦   ¦   ¦   ¦   ¦   +-- Stick1.controller  
¦   ¦   ¦   ¦   ¦   +-- Stick2.controller  
¦   ¦   ¦   ¦   ¦   +-- Stick3.controller  
¦   ¦   ¦   ¦   +-- Pinatas  
¦   ¦   ¦   ¦   ¦   +-- pinataHorse  
¦   ¦   ¦   ¦   ¦   ¦   +-- AnimationHorse.anim  
¦   ¦   ¦   ¦   ¦   ¦   +-- AnimationPinataHorseBom.anim  
¦   ¦   ¦   ¦   ¦   ¦   +-- p1.png  
¦   ¦   ¦   ¦   ¦   ¦   +-- p2.png  
¦   ¦   ¦   ¦   ¦   ¦   +-- p3.png  
¦   ¦   ¦   ¦   ¦   ¦   +-- p4.png  
¦   ¦   ¦   ¦   ¦   ¦   +-- p5.png  
¦   ¦   ¦   ¦   ¦   ¦   +-- p6.png  
¦   ¦   ¦   ¦   ¦   ¦   +-- p7.png  
¦   ¦   ¦   ¦   ¦   ¦   +-- p8.png  
¦   ¦   ¦   ¦   ¦   ¦   +-- piñata-caballo _P_.png  
¦   ¦   ¦   ¦   ¦   ¦   +-- PinataHorse.controller  
¦   ¦   ¦   ¦   ¦   +-- pinataSpecial  
¦   ¦   ¦   ¦   ¦   ¦   +-- a1.png  
¦   ¦   ¦   ¦   ¦   ¦   +-- a2.png  
¦   ¦   ¦   ¦   ¦   ¦   +-- a3.png  
¦   ¦   ¦   ¦   ¦   ¦   +-- a4.png  
¦   ¦   ¦   ¦   ¦   ¦   +-- a5.png  
¦   ¦   ¦   ¦   ¦   ¦   +-- a6.png  
¦   ¦   ¦   ¦   ¦   ¦   +-- a7.png  
¦   ¦   ¦   ¦   ¦   ¦   +-- AnimationPinataSpecialBom.anim  
¦   ¦   ¦   ¦   ¦   ¦   +-- AnimationSpecial.anim  
¦   ¦   ¦   ¦   ¦   ¦   +-- piñata-especial _a_.png  
¦   ¦   ¦   ¦   ¦   ¦   +-- PinataSpecial.controller  
¦   ¦   ¦   ¦   ¦   +-- pinataTraditional  
¦   ¦   ¦   ¦   ¦       +-- AnimationPinataTraditionalBom.anim  
¦   ¦   ¦   ¦   ¦       +-- AnimationTraditional.anim  
¦   ¦   ¦   ¦   ¦       +-- f1.png  
¦   ¦   ¦   ¦   ¦       +-- f2.png  
¦   ¦   ¦   ¦   ¦       +-- f3.png  
¦   ¦   ¦   ¦   ¦       +-- f4.png  
¦   ¦   ¦   ¦   ¦       +-- f5.png  
¦   ¦   ¦   ¦   ¦       +-- PinataTraditional.controller  
¦   ¦   ¦   ¦   +-- StarAreaStick.png  
¦   ¦   ¦   ¦   +-- Stick1.png  
¦   ¦   ¦   ¦   +-- Stick2.png  
¦   ¦   ¦   ¦   +-- Stick3.png  
¦   ¦   ¦   +-- FinalChallenge  
¦   ¦   ¦   ¦   +-- Background.jpg  
¦   ¦   ¦   +-- Generics  
¦   ¦   ¦   ¦   +-- background.png  
¦   ¦   ¦   ¦   +-- botónMenu.png  
¦   ¦   ¦   ¦   +-- BtnBoton.png  
¦   ¦   ¦   ¦   +-- Globe.png  
¦   ¦   ¦   ¦   +-- GrandMother.png  
¦   ¦   ¦   ¦   +-- PopUp.png  
¦   ¦   ¦   ¦   +-- Quenda-Medium.otf  
¦   ¦   ¦   ¦   +-- scores  
¦   ¦   ¦   ¦       +-- starArea.png  
¦   ¦   ¦   ¦       +-- star.png  
¦   ¦   ¦   +-- InitScreen  
¦   ¦   ¦   ¦   +-- background.jpg  
¦   ¦   ¦   +-- MainMenu  
¦   ¦   ¦   ¦   +-- Animations  
¦   ¦   ¦   ¦   ¦   +-- AnimationChallenge1.anim  
¦   ¦   ¦   ¦   ¦   +-- AnimationChallenge2.anim  
¦   ¦   ¦   ¦   ¦   +-- AnimationChallenge3.anim  
¦   ¦   ¦   ¦   ¦   +-- AnimationChallenge4.anim  
¦   ¦   ¦   ¦   ¦   +-- AnimationChallengeFinal.anim  
¦   ¦   ¦   ¦   ¦   +-- BtnChallenge1.controller  
¦   ¦   ¦   ¦   ¦   +-- BtnChallenge2.controller  
¦   ¦   ¦   ¦   ¦   +-- BtnChallenge3.controller  
¦   ¦   ¦   ¦   ¦   +-- BtnChallenge4.controller  
¦   ¦   ¦   ¦   ¦   +-- BtnFinalChallenge.controller  
¦   ¦   ¦   ¦   ¦   +-- genericUI.controller  
¦   ¦   ¦   ¦   +-- Background.jpg  
¦   ¦   ¦   ¦   +-- Challenge1.png  
¦   ¦   ¦   ¦   +-- Challenge2.png  
¦   ¦   ¦   ¦   +-- Challenge3.png  
¦   ¦   ¦   ¦   +-- FinalChallenge.png  
¦   ¦   ¦   ¦   +-- genericUI  
¦   ¦   ¦   ¦   ¦   +-- DropDownMenu  
¦   ¦   ¦   ¦   ¦   ¦   +-- Credits.png  
¦   ¦   ¦   ¦   ¦   ¦   +-- DataSheet.png  
¦   ¦   ¦   ¦   ¦   ¦   +-- DropDownMenuAnimation.anim  
¦   ¦   ¦   ¦   ¦   ¦   +-- dropDownMenu.prefab  
¦   ¦   ¦   ¦   ¦   ¦   +-- glosario08.png  
¦   ¦   ¦   ¦   ¦   ¦   +-- MenuClose.anim  
¦   ¦   ¦   ¦   ¦   ¦   +-- menuContainer.controller  
¦   ¦   ¦   ¦   ¦   ¦   +-- MenuOpen.anim  
¦   ¦   ¦   ¦   ¦   ¦   +-- New Animation213213.anim  
¦   ¦   ¦   ¦   ¦   +-- genericUI.prefab  
¦   ¦   ¦   ¦   +-- reto4.png  
¦   ¦   ¦   +-- Scripts  
¦   ¦   ¦       +-- DropDownMenu.cs  
¦   ¦   ¦       +-- InitOda12.cs  
¦   ¦   ¦       +-- MenuOda12.cs  
¦   ¦   ¦       +-- Reto1Oda12.cs  
¦   ¦   ¦       +-- Reto2Oda12.cs  
¦   ¦   ¦       +-- Reto3Oda12.cs  
¦   ¦   ¦       +-- Reto4Oda12.cs  
¦   ¦   ¦       +-- RetoFinalOda12.cs  
¦   ¦   ¦       +-- SoundManager.cs  
¦   ¦   +-- Scenes  
¦   ¦   ¦   +-- ODA12.unity  
¦   ¦   +-- StreamingAssets  
¦   ¦       +-- ActividadesRolesFases-TSP.xlsx  
¦   ¦       +-- Sound_answerSelected.mp3  
¦   ¦       +-- Sound_closePopUp.mp3  
¦   ¦       +-- Sound_crashAndBoing.mp3  
¦   ¦       +-- Sound_endChallenge.mp3  
¦   ¦       +-- Sound_error.mp3  
¦   ¦       +-- Sound_evaluation.mp3  
¦   ¦       +-- Sound_Fondoevaluacion.mp3  
¦   ¦       +-- Sound_openPopUp.mp3  
¦   ¦       +-- Sound_pinata.mp3  
¦   ¦       +-- Sound_SeleccionarRespuesta.mp3  
¦   ¦       +-- Sound_success.mp3  
¦   ¦       +-- Tropical_Jazz_Paradise.mp3  
¦   +-- Library  
¦   ¦   +-- AnnotationManager  
¦   ¦   +-- assetDatabase3  
¦   ¦   +-- AssetImportState  
¦   ¦   +-- AssetServerCacheV3  
¦   ¦   +-- AssetVersioning.db  
¦   ¦   +-- BuildPlayer.prefs  
¦   ¦   +-- BuildSettings.asset  
¦   ¦   +-- CurrentLayout.dwlt  
¦   ¦   +-- CurrentMaximizeLayout.dwlt  
¦   ¦   +-- EditorUserBuildSettings.asset  
¦   ¦   +-- EditorUserSettings.asset  
¦   ¦   +-- expandedItems  
¦   ¦   +-- InspectorExpandedItems.asset  
¦   ¦   +-- LastSceneManagerSetup.txt  
¦   ¦   +-- LibraryFormatVersion.txt  
¦   ¦   +-- metadata  
¦   ¦   +-- MonoManager.asset  
¦   ¦   +-- ProjectSettings.asset  
¦   ¦   +-- ScriptAssemblies  
¦   ¦   ¦   +-- Assembly-CSharp.dll  
¦   ¦   ¦   +-- Assembly-CSharp.dll.mdb  
¦   ¦   ¦   +-- BuiltinAssemblies.stamp  
¦   ¦   +-- ScriptMapper  
¦   ¦   +-- ShaderCache  
¦   ¦   ¦   +-- 0  
¦   ¦   +-- ShaderCache.db  
¦   ¦   +-- UnityAssemblies  
¦   ¦       +-- Mono.Cecil.dll  
¦   ¦       +-- Mono.Cecil.xml  
¦   ¦       +-- UnityEditor.dll  
¦   ¦       +-- UnityEditor.iOS.Extensions.Common.dll  
¦   ¦       +-- UnityEditor.iOS.Extensions.Common.xml  
¦   ¦       +-- UnityEditor.iOS.Extensions.Xcode.dll  
¦   ¦       +-- UnityEditor.iOS.Extensions.Xcode.xml  
¦   ¦       +-- UnityEditor.xml  
¦   ¦       +-- UnityEngine.dll  
¦   ¦       +-- UnityEngine.Networking.dll  
¦   ¦       +-- UnityEngine.Networking.xml  
¦   ¦       +-- UnityEngine.UI.dll  
¦   ¦       +-- UnityEngine.UI.xml  
¦   ¦       +-- UnityEngine.xml  
¦   ¦       +-- version.txt  
¦   +-- ProjectSettings  
¦       +-- AudioManager.asset  
¦       +-- ClusterInputManager.asset  
¦       +-- DynamicsManager.asset  
¦       +-- EditorBuildSettings.asset  
¦       +-- EditorSettings.asset  
¦       +-- GraphicsSettings.asset  
¦       +-- InputManager.asset  
¦       +-- NavMeshAreas.asset  
¦       +-- NetworkManager.asset  
¦       +-- Physics2DSettings.asset  
¦       +-- ProjectSettings.asset  
¦       +-- ProjectVersion.txt  
¦       +-- QualitySettings.asset  
¦       +-- TagManager.asset  
¦       +-- TimeManager.asset  
¦       +-- UnityAdsSettings.asset  
¦       +-- UnityConnectSettings.asset  
+-- README.md  
  
422 directories, 4848 files    
