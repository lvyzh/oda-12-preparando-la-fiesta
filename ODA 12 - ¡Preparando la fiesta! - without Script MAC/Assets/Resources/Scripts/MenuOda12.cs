﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.Resources.Scripts
{
    public class MenuOda12 : MonoBehaviour
    {
        private bool _actives;
        private Button _btnChallenge1;
        private Button _btnChallenge2;
        private Button _btnChallenge3;
        private Button _btnChallenge4;
        private Button _btnFinalChallenge;
        private GameObject _challenge1;
        private GameObject _challenge2;
        private GameObject _challenge3;
        private GameObject _challenge4;
        private GameObject _clon;
        private GameObject _finalChallenge;
        private GameObject _mainMenu;
        private SoundManager _soundManager;
        private float _timeAnimations;
        //private static string "estrellasODA12_ODA12" = "estrellasODA12_ODA12";
        private int backUpCount;

        public void setBackUpCount(int value)
        {
            backUpCount = value;
        }

        public int getBackUpCount()
        {
            return backUpCount;
        }

        internal void Start()
        {
            _soundManager = transform.GetComponent<SoundManager>();
            
            _actives = false;
            _timeAnimations = 1.0f;
            //GET Main Nodes for ODA12
            _mainMenu = transform.GetChild(1).gameObject;
            _challenge1 = transform.GetChild(2).gameObject;
            _challenge2 = transform.GetChild(3).gameObject;
            _challenge3 = transform.GetChild(4).gameObject;
            _challenge4 = transform.GetChild(5).gameObject;
            _finalChallenge = transform.GetChild(6).gameObject;
            //SET camera for Main Nodes
            _mainMenu.transform.GetChild(0).GetComponent<Canvas>().worldCamera = Camera.main;
            _challenge1.transform.GetChild(0).GetComponent<Canvas>().worldCamera = Camera.main;
            _challenge2.transform.GetChild(0).GetComponent<Canvas>().worldCamera = Camera.main;
            _challenge3.transform.GetChild(0).GetComponent<Canvas>().worldCamera = Camera.main;
            _challenge4.transform.GetChild(0).GetComponent<Canvas>().worldCamera = Camera.main;
            //GETNODES for MainMenu
            _btnChallenge1 = _mainMenu.transform.GetChild(0).FindChild("BtnChallenge1").GetComponent<Button>();
            _btnChallenge1.onClick.AddListener(delegate
            {
                _btnChallenge1.transform.GetComponent<Animator>().Play("AnimationChallenge1");
                StartCoroutine(WaitChangeChanllenge(1));
            });
            _btnChallenge2 = _mainMenu.transform.GetChild(0).FindChild("BtnChallenge2").GetComponent<Button>();
            _btnChallenge2.onClick.AddListener(delegate
            {
                _btnChallenge2.transform.GetComponent<Animator>().Play("AnimationChallenge2");
                StartCoroutine(WaitChangeChanllenge(2));
            });
            _btnChallenge3 = _mainMenu.transform.GetChild(0).FindChild("BtnChallenge3").GetComponent<Button>();
            _btnChallenge3.onClick.AddListener(delegate
            {
                _btnChallenge3.transform.GetComponent<Animator>().Play("AnimationChallenge3");
                StartCoroutine(WaitChangeChanllenge(3));
            });
            _btnChallenge4 = _mainMenu.transform.GetChild(0).FindChild("BtnChallenge4").GetComponent<Button>();
            _btnChallenge4.onClick.AddListener(delegate
            {
                _btnChallenge4.transform.GetComponent<Animator>().Play("AnimationChallenge4");
                StartCoroutine(WaitChangeChanllenge(4));
            });
            _btnFinalChallenge = _mainMenu.transform.GetChild(0).FindChild("BtnFinalChallenge").GetComponent<Button>();
            _btnFinalChallenge.onClick.AddListener(delegate
            {
                _btnFinalChallenge.transform.GetComponent<Animator>().Play("AnimationChallengeFinal");
                StartCoroutine(WaitChangeChanllenge(5));
            });
        }

        public void GoToMenu()
        {
            _actives = false;
            transform.GetChild(0).gameObject.SetActive(false); //portada
            transform.GetChild(1).gameObject.SetActive(true); //menu
            transform.GetChild(2).gameObject.SetActive(false); //1
            transform.GetChild(3).gameObject.SetActive(false); //2
            transform.GetChild(4).gameObject.SetActive(false); //3
            transform.GetChild(5).gameObject.SetActive(false); //4
            transform.GetChild(6).gameObject.SetActive(false); //reto final 
            _clon.SetActive(true);
            Destroy(gameObject);
        }

        public bool IsActiveChallenge()
        {
            return _actives;
        }

        private IEnumerator WaitChangeChanllenge(int option)
        {
            yield return new WaitForSeconds(_timeAnimations);

            _clon = Instantiate(gameObject);
            _clon.SetActive(false);
            PlaySoundOpenPopUp();
            _mainMenu.SetActive(false);
            _actives = true;
            switch (option)
            {
                case 1:
                    _challenge1.SetActive(true);
                    break;
                case 2:
                    _challenge2.SetActive(true);
                    break;
                case 3:
                    _challenge3.SetActive(true);
                    break;
                case 4:
                    _challenge4.SetActive(true);
                    break;
                case 5:
                    _finalChallenge.SetActive(true);
                    break;
            }
        }

        private void PlaySoundOpenPopUp()
        {
            _soundManager.PlaySound("Sound_openPopUp", 1);
            _soundManager.PlaySound("Tropical_Jazz_Paradise", 0);
        }
    }
}