﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.Resources.Scripts
{
    public class Reto4Oda12 : MonoBehaviour
    {
        private Animator _animatorCandies;
        private Animator _animatorPinata;
        private Animator _animatorStick;
        private Button _btnAcept;
        private Button _btnStick1;
        private Button _btnStick2;
        private Button _btnStick3;
        private GameObject _challenge4;
        private GameObject _finalChallenge;
        private int _hits;
        private int _index;
        private string[] _msgPopUpIndicationStrings;
        private string _nameAnimation;
        private string _nameAnimationBom;
        private string _namePinata;
        private string _nameStarsVar;
        private string _nameAnimationStick;
        private GameObject _popUpIndications;
        private int _randomHit;
        private GameObject _score;
        private SoundManager _soundManager;
        private int _starCountGame;
        private GameObject _sticks;
        private float _timeAnimations;
        private Text _txtMessage;
        private Text _txtScore;

        internal void Start()
        {
            _nameStarsVar = "estrellasODA12_ODA12";
            _timeAnimations = 2.0f;
            _soundManager = transform.GetComponent<SoundManager>();
            _hits = 0;
            _challenge4 = transform.GetChild(5).gameObject;
            _finalChallenge = transform.GetChild(6).gameObject;
            _score = _challenge4.transform.GetChild(0).FindChild("Score").gameObject;
            _txtScore = _score.transform.FindChild("TxtScore").GetComponent<Text>();
            _sticks = _challenge4.transform.GetChild(0).FindChild("Sticks").gameObject;
            _popUpIndications = _challenge4.transform.GetChild(0).FindChild("PopUpIndications").gameObject;
            _txtMessage = _popUpIndications.transform.FindChild("TxtMessage").GetComponent<Text>();
            _btnAcept = _popUpIndications.transform.FindChild("BtnAcept").GetComponent<Button>();
            _btnAcept.onClick.AddListener(delegate
            {
                PlaySoundClosePopUp();
                _popUpIndications.SetActive(false);
                CheckStars();
            });
            _btnStick1 = _sticks.transform.FindChild("Stick1").GetComponent<Button>();
            _btnStick1.onClick.AddListener(delegate { BuySticks(0, 18); });
            _btnStick2 = _sticks.transform.FindChild("Stick2").GetComponent<Button>();
            _btnStick2.onClick.AddListener(delegate { BuySticks(1, 12); });
            _btnStick3 = _sticks.transform.FindChild("Stick3").GetComponent<Button>();
            _btnStick3.onClick.AddListener(delegate { BuySticks(2, 6); });
            _animatorCandies = _challenge4.transform.GetChild(0).FindChild("Candies").GetComponent<Animator>();
            SetUpVariables();
            ShowPopUpIndications(0);
        }

        internal void Update()
        {
            _txtScore.text = _starCountGame.ToString();
        }

        private void ShowPopUpIndications(int indexMessage)
        {
            PlaySoundOpenPopUp();
            _popUpIndications.SetActive(true);
            _txtMessage.text = _msgPopUpIndicationStrings[indexMessage];
        }

        private void SetUpVariables()
        {
            _msgPopUpIndicationStrings = new string[5];
            _msgPopUpIndicationStrings[0] =
                "Va a empezar la mejor parte de la fiesta: ¡romper la piñata! Con las estrellas que reuniste escoge el palo que más te gusta. ";
            _msgPopUpIndicationStrings[1] =
                "Vuelve a intentar los retos para sumar más estrellas y llevarte un palo para romper la piñata. ";
            _msgPopUpIndicationStrings[2] = "Toca el palo todas las veces necesarias para que se rompa la piñata.";
            _msgPopUpIndicationStrings[3] = "¡Sigamos con el reto final!";
            _msgPopUpIndicationStrings[4] = "No cuentas con suficientes estrellas. Vuelve a intentar los retos para sumar más estrellas.";
        }

        private void CheckStars()
        {
            _starCountGame = PlayerPrefs.GetInt(_nameStarsVar);
            _btnAcept.onClick.RemoveAllListeners();
            _btnAcept.onClick.AddListener(delegate
            {
                PlaySoundClosePopUp();
                _popUpIndications.SetActive(false);
            });
            if (_starCountGame < 6)
            {
                ShowPopUpIndications(1);
            }
            else
            {
                _sticks.SetActive(true);
                _score.SetActive(true);
            }
        }

        private void BuySticks(int stick, int price)
        {
            if (_starCountGame < price)
            {
                ShowPopUpIndications(4);
            }
            else
            {
                _namePinata = PlayerPrefs.GetString("PinataName");
                _starCountGame -= price;
                _sticks.transform.FindChild("PinatasBroken")
                    .transform.FindChild(_namePinata)
                    .transform.gameObject.SetActive(true);
                _animatorPinata =
                    _sticks.transform.FindChild("PinatasBroken")
                        .transform.FindChild(_namePinata)
                        .GetComponent<Animator>();
                _btnAcept.onClick.AddListener(delegate
                {
                    _popUpIndications.SetActive(false);
                    switch (_namePinata)
                    {
                        case "PinataHorse":
                            _nameAnimation = "AnimationHorse";
                            _nameAnimationBom = "AnimationPinataHorseBom";
                            break;
                        case "PinataSpecial":
                            _nameAnimation = "AnimationSpecial";
                            _nameAnimationBom = "AnimationPinataSpecialBom";
                            break;
                        case "PinataTraditional":
                            _nameAnimation = "AnimationTraditional";
                            _nameAnimationBom = "AnimationPinataTraditionalBom";
                            break;
                    }
                });
                switch (stick)
                {
                    case 0:
                        _index = 0;
                        _btnStick1.transform.GetChild(0).gameObject.SetActive(false);
                        _btnStick2.transform.gameObject.SetActive(false);
                        _btnStick3.transform.gameObject.SetActive(false);
                        _nameAnimationStick = "AnimationStick1";
                        _animatorStick = _btnStick1.GetComponent<Animator>();
                        _btnStick1.onClick.RemoveAllListeners();
                        _btnStick1.onClick.AddListener(BrokenPinata);
                        break;
                    case 1:
                        _index = 1;
                        _btnStick2.transform.GetChild(0).gameObject.SetActive(false);
                        _btnStick1.transform.gameObject.SetActive(false);
                        _btnStick3.transform.gameObject.SetActive(false);
                        _nameAnimationStick = "AnimationStick2";
                        _animatorStick = _btnStick2.GetComponent<Animator>();
                        _btnStick2.onClick.RemoveAllListeners();
                        _btnStick2.onClick.AddListener(BrokenPinata);
                        break;
                    case 2:
                        _index = 2;
                        _btnStick3.transform.GetChild(0).gameObject.SetActive(false);
                        _btnStick1.transform.gameObject.SetActive(false);
                        _btnStick2.transform.gameObject.SetActive(false);
                        _nameAnimationStick = "AnimationStick3";
                        _animatorStick = _btnStick3.GetComponent<Animator>();
                        _btnStick3.onClick.RemoveAllListeners();
                        _btnStick3.onClick.AddListener(BrokenPinata);
                        break;
                }
                _randomHit = Random.Range(4, 8);
                ShowPopUpIndications(2);
            }
        }

        private void BrokenPinata()
        {
            _animatorStick.Play(_nameAnimationStick);
            _animatorPinata.Play(_nameAnimation);
            PlaySoundCrash();
            if (_hits < _randomHit)
            {
                _hits++;
            }
            else
            {
                _animatorStick.Stop();
                _animatorPinata.Play(_nameAnimationBom);
                _nameAnimation = "AnimationCandies";
                _animatorCandies.Play(_nameAnimation);
                StartCoroutine(Wait0());
                _btnAcept.onClick.AddListener(delegate
                {
                    PlaySoundClosePopUp();
                    _popUpIndications.SetActive(false);
                    _challenge4.SetActive(false);
                    PlaySoundEndChallenge();
                    _finalChallenge.SetActive(true);
                });
                switch (_index)
                {
                    case 0:
                        _btnStick1.onClick.RemoveAllListeners();
                        break;
                    case 1:
                        _btnStick2.onClick.RemoveAllListeners();
                        break;
                    case 2:
                        _btnStick3.onClick.RemoveAllListeners();
                        break;
                }
            }
        }

        private IEnumerator Wait0()
        {
            yield return new WaitForSeconds(_timeAnimations + 1.0f);
            _sticks.transform.FindChild("PinatasBroken")
                .transform.FindChild(_namePinata)
                .transform.gameObject.SetActive(false);
            _nameAnimation = "AnimationCandies1";
            //_animatorCandies.Play(_nameAnimation);
            StartCoroutine(Wait());
        }

        private IEnumerator Wait()
        {
            yield return new WaitForSeconds(_timeAnimations + 1.0f);
            _challenge4.transform.GetChild(0).FindChild("Candies").gameObject.SetActive(false);
            _sticks.SetActive(false);
            ShowPopUpIndications(3);
            PlayerPrefs.SetInt(_nameStarsVar, 0);
        }

        private void PlaySoundCrash()
        {
            _soundManager.PlaySound("Sound_crashAndBoing", 1);
        }

        private void PlaySoundOpenPopUp()
        {
            _soundManager.PlaySound("Sound_openPopUp", 3);
        }

        private void PlaySoundClosePopUp()
        {
            _soundManager.PlaySound("Sound_closePopUp", 4);
        }

        private void PlaySoundEndChallenge()
        {
            _soundManager.PlaySound("Sound_endChallenge", 2);
        }
    }
}