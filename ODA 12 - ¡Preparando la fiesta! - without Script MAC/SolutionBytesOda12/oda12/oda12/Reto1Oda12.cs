﻿using System;
using UnityEngine;
using UnityEngine.UI;
using Random = UnityEngine.Random;

namespace Assets.Resources.Scripts
{
    public class Reto1Oda12 : MonoBehaviour
    {
        private bool _animationStarted;
        private string[] _arrayTeddiesNamesAll;
        private int[] _arrayTeddiesNumbersPricesAll;
        private int[] _arrayTeddiesNumbersPricesGame;
        private string[] _arrayToysNamesAll;
        private int[] _arrayToysNumbersPricesAll;
        private int[] _arrayToysNumbersPricesGame;
        private bool _banActivity1;
        private bool _banPopUpNextActivity;
        private Button _btnAceptPopUpIndications;
        private Button _btnAceptPopUpNextActivity;
        private Button _btnAnswerA;
        private Button _btnAnswerB;
        private Button _btnAnswerC;
        private Button _btnGlobePopUpNextActivity;
        private GameObject _challenge1;
        private GameObject _challenge2;
        private int _countPositionSuccessTeddy;
        private int _countPositionSuccessToys;
        private bool _dragging;
        private bool _draggingTeddy;
        private bool _dragginToys;
        private GameObject _elementPosition;
        private Image _imgAirPlane;
        private Image _imgAirPlanePrice;
        private Image _imgBook;
        private Image _imgBookPrice;
        private Image _imgCar;
        private Image _imgCarPrice;
        private Image _imgDollHouse;
        private Image _imgDollHousePrice;
        private Image _imgMovie;
        private Image _imgMoviePrice;
        private Image _imgPiano;
        private Image _imgPianoPrice;
        private Vector3 _imgPosition1;
        private Vector3 _imgPosition1Label;
        private GameObject _imgPosition1Price;
        private GameObject _imgPosition1Toy;
        private Vector3 _imgPosition2;
        private Vector3 _imgPosition2Label;
        private GameObject _imgPosition2Price;
        private GameObject _imgPosition2Toy;
        private Vector3 _imgPosition3;
        private Vector3 _imgPosition3Label;
        private GameObject _imgPosition3Price;
        private GameObject _imgPosition3Toy;
        private Vector3 _imgPosition4;
        private Vector3 _imgPosition4Label;
        private GameObject _imgPosition4Price;
        private GameObject _imgPosition4Toy;
        private GameObject _imgPrice1PosPriceOrderBy;
        private GameObject _imgPrice2PosPriceOrderBy;
        private GameObject _imgPrice3PosPriceOrderBy;
        private GameObject _imgPrice4PosPriceOrderBy;
        private Image _imgRobot;
        private Image _imgRobotPrice;
        private Image _imgSoccerBall;
        private Image _imgSoccerBallPrice;
        private Image _imgTeddy1;
        private Image _imgTeddy1Price1;
        private Image _imgTeddy1Price2;
        private Image _imgTeddy1Price3;
        private Image _imgTeddy2;
        private Image _imgTeddy2Price1;
        private Image _imgTeddy2Price2;
        private Image _imgTeddy2Price3;
        private Image _imgTeddy3;
        private Image _imgTeddy3Price1;
        private Image _imgTeddy3Price2;
        private Image _imgTeddy3Price3;
        private Image _imgTeddy4;
        private Image _imgTeddy4Price1;
        private Image _imgTeddy4Price2;
        private Image _imgTeddy4Price3;
        private Image _imgVideoGame;
        private Image _imgVideoGamePrice;
        private int _indexTxtMessagesOptionsPopUps;
        private string _nameImg1;
        private string _nameImg2;
        private string _nameImg3;
        private string _nameImg4;
        private string _nameStarsVar;
        private GameObject _optionsPopUps;
        private int[] _optionsSuccessOptionsPopUps;
        private GameObject _popUpIndications;
        private GameObject _popUpNextActivity;
        private Vector3 _positionInit;
        private GameObject _score;
        private SoundManager _soundManager;
        private int _starCountGame;
        private string[] _stgMessagePopUpIndications;
        private string _stgMessagePopUpNextActivity;
        private GameObject _teddies;
        private string _text;
        private float _timeAnimation;
        private float _timeTotal;
        private float _timeTxtMessagePopUpNextActivity;
        private GameObject _toys;
        private GameObject _toysOrderByPrice;
        private Text _txtMessageCurrentOptionsPopUps;
        private Text _txtMessagePopUpIndications;
        private Text _txtMessagePopUpNextActivity;
        private string[] _txtMessagesButtonsOptionsPopUps;
        private string[] _txtMessagesErrorsOptionsPopUps;
        private string[] _txtMessagesOptionsPopUps;
        private Text _txtScore;
        private int[] _valuesOccupied;

        internal void Start()
        {
            _nameStarsVar = "estrellasODA12_ODA12";
            _soundManager = transform.GetComponent<SoundManager>();
            _challenge1 = transform.GetChild(2).gameObject;
            _challenge2 = transform.GetChild(3).gameObject;
            SetUpValues();
            //popUpNextActivity
            _popUpNextActivity = _challenge1.transform.GetChild(0).FindChild("PopUpNextActivity").gameObject;
            _btnGlobePopUpNextActivity =
                _popUpNextActivity.transform.FindChild("PopUp").transform.FindChild("PopUpImage").GetComponent<Button>();
            _btnGlobePopUpNextActivity.onClick.AddListener(delegate
            {
                if (_banPopUpNextActivity) return;
                AnimatedString(_timeTxtMessagePopUpNextActivity, _stgMessagePopUpNextActivity);
                Invoke("SetEnableBtnAceptNextActivity", _timeTxtMessagePopUpNextActivity + 0.5f);
                _banPopUpNextActivity = true;
            });
            _btnAceptPopUpNextActivity =
                _popUpNextActivity.transform.FindChild("PopUp").transform.FindChild("BtnAcept").GetComponent<Button>();
            _btnAceptPopUpNextActivity.onClick.AddListener(delegate
            {
                PlaySoundClosePopUp();
                _popUpNextActivity.SetActive(false);
                ShowPopUpIndications(0);
            });
            _txtMessagePopUpNextActivity =
                _btnGlobePopUpNextActivity.transform.FindChild("TxtMessage").GetComponent<Text>();
            _popUpNextActivity.SetActive(true);
            //popUpIndication
            _popUpIndications = _challenge1.transform.GetChild(0).FindChild("PopUpIndications").gameObject;
            _txtMessagePopUpIndications = _popUpIndications.transform.FindChild("TxtMessage").GetComponent<Text>();
            _btnAceptPopUpIndications = _popUpIndications.transform.FindChild("BtnAcept").GetComponent<Button>();
            _btnAceptPopUpIndications.onClick.AddListener(delegate
            {
                PlaySoundClosePopUp();
                _popUpIndications.SetActive(false);
                if (_banActivity1) return;
                ShowActivity1();
                _banActivity1 = true;
            });
            //toys
            _toys = _challenge1.transform.GetChild(0).FindChild("Toys").gameObject;
            _imgSoccerBall = _toys.transform.FindChild("ImgSoccerBall").GetComponent<Image>();
            _imgSoccerBallPrice = _toys.transform.FindChild("ImgSoccerBallPrice").GetComponent<Image>();
            _imgBook = _toys.transform.FindChild("ImgBook").GetComponent<Image>();
            _imgBookPrice = _toys.transform.FindChild("ImgBookPrice").GetComponent<Image>();
            _imgDollHouse = _toys.transform.FindChild("ImgDollHouse").GetComponent<Image>();
            _imgDollHousePrice = _toys.transform.FindChild("ImgDollHousePrice").GetComponent<Image>();
            _imgVideoGame = _toys.transform.FindChild("ImgVideoGame").GetComponent<Image>();
            _imgVideoGamePrice = _toys.transform.FindChild("ImgVideoGamePrice").GetComponent<Image>();
            _imgAirPlane = _toys.transform.FindChild("ImgAirPlane").GetComponent<Image>();
            _imgAirPlanePrice = _toys.transform.FindChild("ImgAirPlanePrice").GetComponent<Image>();
            _imgPiano = _toys.transform.FindChild("ImgPiano").GetComponent<Image>();
            _imgPianoPrice = _toys.transform.FindChild("ImgPianoPrice").GetComponent<Image>();
            _imgMovie = _toys.transform.FindChild("ImgMovie").GetComponent<Image>();
            _imgMoviePrice = _toys.transform.FindChild("ImgMoviePrice").GetComponent<Image>();
            _imgRobot = _toys.transform.FindChild("ImgRobot").GetComponent<Image>();
            _imgRobotPrice = _toys.transform.FindChild("ImgRobotPrice").GetComponent<Image>();
            _imgCar = _toys.transform.FindChild("ImgCar").GetComponent<Image>();
            _imgCarPrice = _toys.transform.FindChild("ImgCarPrice").GetComponent<Image>();
            _imgPosition1Toy = _toys.transform.FindChild("ImgPosition1Toy").gameObject;
            _imgPosition1Price = _toys.transform.FindChild("ImgPosition1Label").gameObject;
            _imgPosition2Toy = _toys.transform.FindChild("ImgPosition2Toy").gameObject;
            _imgPosition2Price = _toys.transform.FindChild("ImgPosition2Label").gameObject;
            _imgPosition3Toy = _toys.transform.FindChild("ImgPosition3Toy").gameObject;
            _imgPosition3Price = _toys.transform.FindChild("ImgPosition3Label").gameObject;
            _imgPosition4Toy = _toys.transform.FindChild("ImgPosition4Toy").gameObject;
            _imgPosition4Price = _toys.transform.FindChild("ImgPosition4Label").gameObject;
            _toysOrderByPrice = _challenge1.transform.GetChild(0).FindChild("ToysOrderByPrice").gameObject;
            //get nodes for challenge1 screen 1
            _imgPrice1PosPriceOrderBy = _toysOrderByPrice.transform.FindChild("ImgPrice1Pos").gameObject;
            _imgPrice2PosPriceOrderBy = _toysOrderByPrice.transform.FindChild("ImgPrice2Pos").gameObject;
            _imgPrice3PosPriceOrderBy = _toysOrderByPrice.transform.FindChild("ImgPrice3Pos").gameObject;
            _imgPrice4PosPriceOrderBy = _toysOrderByPrice.transform.FindChild("ImgPrice4Pos").gameObject;
            //teddies
            _teddies = _challenge1.transform.GetChild(0).FindChild("Teddies").gameObject;
            _imgTeddy1 = _teddies.transform.FindChild("ImgTeddy1").GetComponent<Image>();
            _imgTeddy1Price1 = _teddies.transform.FindChild("ImgTeddy1Price1").GetComponent<Image>();
            _imgTeddy1Price2 = _teddies.transform.FindChild("ImgTeddy1Price2").GetComponent<Image>();
            _imgTeddy1Price3 = _teddies.transform.FindChild("ImgTeddy1Price3").GetComponent<Image>();
            _imgTeddy2 = _teddies.transform.FindChild("ImgTeddy2").GetComponent<Image>();
            _imgTeddy2Price1 = _teddies.transform.FindChild("ImgTeddy2Price1").GetComponent<Image>();
            _imgTeddy2Price2 = _teddies.transform.FindChild("ImgTeddy2Price2").GetComponent<Image>();
            _imgTeddy2Price3 = _teddies.transform.FindChild("ImgTeddy2Price3").GetComponent<Image>();
            _imgTeddy3 = _teddies.transform.FindChild("ImgTeddy3").GetComponent<Image>();
            _imgTeddy3Price1 = _teddies.transform.FindChild("ImgTeddy3Price1").GetComponent<Image>();
            _imgTeddy3Price2 = _teddies.transform.FindChild("ImgTeddy3Price2").GetComponent<Image>();
            _imgTeddy3Price3 = _teddies.transform.FindChild("ImgTeddy3Price3").GetComponent<Image>();
            _imgTeddy4 = _teddies.transform.FindChild("ImgTeddy4").GetComponent<Image>();
            _imgTeddy4Price1 = _teddies.transform.FindChild("ImgTeddy4Price1").GetComponent<Image>();
            _imgTeddy4Price2 = _teddies.transform.FindChild("ImgTeddy4Price2").GetComponent<Image>();
            _imgTeddy4Price3 = _teddies.transform.FindChild("ImgTeddy4Price3").GetComponent<Image>();
            //get optioins popups questions
            _optionsPopUps = _challenge1.transform.GetChild(0).FindChild("OptionsPopUps").gameObject;
            _txtMessageCurrentOptionsPopUps = _optionsPopUps.transform.FindChild("TxtMessage").GetComponent<Text>();
            _btnAnswerA = _optionsPopUps.transform.FindChild("BtnAnswerA").GetComponent<Button>();
            _btnAnswerA.onClick.AddListener(delegate { UpdateInfoPopUp(0); });
            _btnAnswerB = _optionsPopUps.transform.FindChild("BtnAnswerB").GetComponent<Button>();
            _btnAnswerB.onClick.AddListener(delegate { UpdateInfoPopUp(1); });
            _btnAnswerC = _optionsPopUps.transform.FindChild("BtnAnswerC").GetComponent<Button>();
            _btnAnswerC.onClick.AddListener(delegate { UpdateInfoPopUp(2); });
            //game stars
            _starCountGame = PlayerPrefs.GetInt(_nameStarsVar);
            //get elements stars
            _score = _challenge1.transform.GetChild(0).FindChild("Score").gameObject;
            _txtScore = _score.transform.FindChild("TxtScore").GetComponent<Text>();
        }

        internal void Update()
        {
            if (Input.touchCount > 0)
            {
                var pos = Camera.main.ScreenToWorldPoint(Input.GetTouch(0).position);
                var hit = Physics2D.Raycast(pos, Vector2.zero);
                if (Input.GetTouch(0).phase == TouchPhase.Began)
                    try
                    {
                        if ((hit.collider.gameObject.name == "ImgSoccerBallPrice") ||
                            (hit.collider.gameObject.name == "ImgBookPrice") ||
                            (hit.collider.gameObject.name == "ImgDollHousePrice") ||
                            (hit.collider.gameObject.name == "ImgVideoGamePrice") ||
                            (hit.collider.gameObject.name == "ImgAirPlanePrice") ||
                            (hit.collider.gameObject.name == "ImgPianoPrice") ||
                            (hit.collider.gameObject.name == "ImgMoviePrice") ||
                            (hit.collider.gameObject.name == "ImgRobotPrice") ||
                            (hit.collider.gameObject.name == "ImgCarPrice") ||
                            (hit.collider.gameObject.name == "ImgTeddy1Price1") ||
                            (hit.collider.gameObject.name == "ImgTeddy1Price2") ||
                            (hit.collider.gameObject.name == "ImgTeddy1Price3") ||
                            (hit.collider.gameObject.name == "ImgTeddy2Price1") ||
                            (hit.collider.gameObject.name == "ImgTeddy2Price2") ||
                            (hit.collider.gameObject.name == "ImgTeddy2Price3") ||
                            (hit.collider.gameObject.name == "ImgTeddy3Price1") ||
                            (hit.collider.gameObject.name == "ImgTeddy3Price2") ||
                            (hit.collider.gameObject.name == "ImgTeddy3Price3") ||
                            (hit.collider.gameObject.name == "ImgTeddy4Price1") ||
                            (hit.collider.gameObject.name == "ImgTeddy4Price2") ||
                            (hit.collider.gameObject.name == "ImgTeddy4Price3"))
                        {
                            _elementPosition = hit.collider.gameObject;
                            _positionInit = hit.collider.gameObject.transform.localPosition;
                            _dragging = true;
                        }
                    }
                    catch (Exception ex)
                    {
                        print(ex);
                    }
                if (Input.GetTouch(0).phase == TouchPhase.Moved)
                    if (_dragging)
                        _elementPosition.transform.position = new Vector3(pos.x, pos.y + 0.5f,
                            _elementPosition.transform.position.z);
                if (Input.GetTouch(0).phase == TouchPhase.Ended)
                    if (_dragging)
                    {
                        if (!_dragginToys)
                        {
                            if ((_elementPosition.name == _nameImg1) &&
                                (Vector3.Distance(_elementPosition.transform.localPosition,
                                     _imgPrice1PosPriceOrderBy.transform.localPosition) < 80))
                            {
                                _elementPosition.transform.localPosition =
                                    _imgPrice1PosPriceOrderBy.transform.localPosition;
                                _countPositionSuccessToys++;
                                _starCountGame += 2;
                                PlayerPrefs.SetInt(_nameStarsVar, _starCountGame);
                                PlaySoundSuccess();
                            }
                            else if ((_elementPosition.name == _nameImg2) &&
                                     (Vector3.Distance(_elementPosition.transform.localPosition,
                                          _imgPrice2PosPriceOrderBy.transform.localPosition) < 80))
                            {
                                _elementPosition.transform.localPosition =
                                    _imgPrice2PosPriceOrderBy.transform.localPosition;
                                _countPositionSuccessToys++;
                                _starCountGame += 2;
                                PlayerPrefs.SetInt(_nameStarsVar, _starCountGame);
                                PlaySoundSuccess    ();
                            }
                            else if ((_elementPosition.name == _nameImg3) &&
                                     (Vector3.Distance(_elementPosition.transform.localPosition,
                                          _imgPrice3PosPriceOrderBy.transform.localPosition) < 80))
                            {
                                _elementPosition.transform.localPosition =
                                    _imgPrice3PosPriceOrderBy.transform.localPosition;
                                _countPositionSuccessToys++;
                                _starCountGame += 2;
                                PlayerPrefs.SetInt(_nameStarsVar, _starCountGame);
                                PlaySoundSuccess();
                            }
                            else if ((_elementPosition.name == _nameImg4) &&
                                     (Vector3.Distance(_elementPosition.transform.localPosition,
                                          _imgPrice4PosPriceOrderBy.transform.localPosition) < 80))
                            {
                                _elementPosition.transform.localPosition =
                                    _imgPrice4PosPriceOrderBy.transform.localPosition;
                                _countPositionSuccessToys++;
                                _starCountGame += 2;
                                PlayerPrefs.SetInt(_nameStarsVar, _starCountGame);
                                PlaySoundSuccess();
                            }
                            else
                            {
                                if ((Vector3.Distance(_elementPosition.transform.localPosition,
                                          _imgPrice4PosPriceOrderBy.transform.localPosition) < 80) ||
                                          (Vector3.Distance(_elementPosition.transform.localPosition,
                                          _imgPrice3PosPriceOrderBy.transform.localPosition) < 80) ||
                                          (Vector3.Distance(_elementPosition.transform.localPosition,
                                          _imgPrice2PosPriceOrderBy.transform.localPosition) < 80) ||
                                          (Vector3.Distance(_elementPosition.transform.localPosition,
                                          _imgPrice1PosPriceOrderBy.transform.localPosition) < 80))
                                    ShowPopUpError();
                                _elementPosition.transform.localPosition = _positionInit;
                            }
                            if (_countPositionSuccessToys == 4)
                            {
                                _countPositionSuccessToys = 0;
                                _dragginToys = true;
                                ShowPopUpSuccessAtivity1();
                            }
                        }
                        if (!_draggingTeddy)
                        {
                            if ((_elementPosition.name == _nameImg1) &&
                                (Vector3.Distance(_elementPosition.transform.localPosition,
                                     _imgPrice1PosPriceOrderBy.transform.localPosition) < 80))
                            {
                                _elementPosition.transform.localPosition =
                                    _imgPrice1PosPriceOrderBy.transform.localPosition;
                                _countPositionSuccessTeddy++;
                                PlaySoundSuccess();
                                _starCountGame += 2;
                                PlayerPrefs.SetInt(_nameStarsVar, _starCountGame);
                            }
                            else if ((_elementPosition.name == _nameImg2) &&
                                     (Vector3.Distance(_elementPosition.transform.localPosition,
                                          _imgPrice2PosPriceOrderBy.transform.localPosition) < 80))
                            {
                                _elementPosition.transform.localPosition =
                                    _imgPrice2PosPriceOrderBy.transform.localPosition;
                                _countPositionSuccessTeddy++;
                                PlaySoundSuccess();
                                _starCountGame += 2;
                                PlayerPrefs.SetInt(_nameStarsVar, _starCountGame);
                            }
                            else if ((_elementPosition.name == _nameImg3) &&
                                     (Vector3.Distance(_elementPosition.transform.localPosition,
                                          _imgPrice3PosPriceOrderBy.transform.localPosition) < 80))
                            {
                                _elementPosition.transform.localPosition =
                                    _imgPrice3PosPriceOrderBy.transform.localPosition;
                                _countPositionSuccessTeddy++;
                                PlaySoundSuccess();
                                _starCountGame += 2;
                                PlayerPrefs.SetInt(_nameStarsVar, _starCountGame);
                            }
                            else if ((_elementPosition.name == _nameImg4) &&
                                     (Vector3.Distance(_elementPosition.transform.localPosition,
                                          _imgPrice4PosPriceOrderBy.transform.localPosition) < 80))
                            {
                                _elementPosition.transform.localPosition =
                                    _imgPrice4PosPriceOrderBy.transform.localPosition;
                                _countPositionSuccessTeddy++;
                                PlaySoundSuccess();
                                _starCountGame += 2;
                                PlayerPrefs.SetInt(_nameStarsVar, _starCountGame);
                            }
                            else
                            {
                                if ((Vector3.Distance(_elementPosition.transform.localPosition,
                                          _imgPrice4PosPriceOrderBy.transform.localPosition) < 80) ||
                                          (Vector3.Distance(_elementPosition.transform.localPosition,
                                          _imgPrice3PosPriceOrderBy.transform.localPosition) < 80) ||
                                          (Vector3.Distance(_elementPosition.transform.localPosition,
                                          _imgPrice2PosPriceOrderBy.transform.localPosition) < 80) ||
                                          (Vector3.Distance(_elementPosition.transform.localPosition,
                                          _imgPrice1PosPriceOrderBy.transform.localPosition) < 80))
                                    ShowPopUpError();
                                _elementPosition.transform.localPosition = _positionInit;
                            }
                            if (_countPositionSuccessTeddy == 4)
                            {
                                _countPositionSuccessTeddy = 0;
                                _draggingTeddy = true;
                                PopUpsOptionsQuestions();
                            }
                        }
                        _dragging = false;
                    }
            }

            _txtScore.text = _starCountGame.ToString();
            //siempre al final de la animacion
            if (!_animationStarted) return;
            _timeAnimation -= Time.deltaTime;
            if (_timeAnimation <= 0) _timeAnimation = 0;
            _txtMessagePopUpNextActivity.text = _text.Substring(0,
                (int) ((_timeTotal - _timeAnimation)*_text.Length/_timeTotal));
        }

        private void SetUpValues()
        {
            _timeTxtMessagePopUpNextActivity = 9.0f;
            _animationStarted = false;
            _timeAnimation = 0.0f;
            _timeTotal = 0.0f;
            _banActivity1 = false;
            _dragging = false;
            _dragginToys = false;
            _draggingTeddy = true;
            _countPositionSuccessToys = 0;
            _countPositionSuccessTeddy = 0;
            _indexTxtMessagesOptionsPopUps = 0;
            //popUpNextActivity
            _stgMessagePopUpNextActivity =
                "Quiero mucho a mi nieto Carlos, ¿me ayudarías a organizarle su fiesta de cumpleaños? ";
            _banPopUpNextActivity = false;
            //popUpIndications
            _stgMessagePopUpIndications = new string[9];
            _stgMessagePopUpIndications[0] =
                "Se le olvidaron sus lentes a la abuela. Arrastra las etiquetas de los juguetes y colócalas de la más barata hasta la más cara así le quedarán más claros los precios.";
            _valuesOccupied = new int[4];
            _arrayToysNumbersPricesGame = new int[4];
            _arrayToysNumbersPricesAll = new int[9];
            _arrayToysNamesAll = new string[9];
            //popUpErrorActivity1
            _stgMessagePopUpIndications[1] =
                "Observa atentamente los números, compara cada cifra, de izquierda a derecha.";
            //popupSuccessActivity1
            _stgMessagePopUpIndications[2] = "La abuela no está muy convencida, prefiere ir a ver peluches.";
            //popupShowActivity2
            _stgMessagePopUpIndications[3] =
                "Arrastra las etiquetas de los juguetes y colócalas de la más barata hasta la más cara para que le queden más claros los precios a la abuela.";
            _arrayTeddiesNumbersPricesGame = new int[4];
            _arrayTeddiesNumbersPricesAll = new int[12];
            _arrayTeddiesNamesAll = new string[12];
            //questions
            _txtMessagesOptionsPopUps = new string[3];
            _txtMessagesButtonsOptionsPopUps = new string[9];
            _txtMessagesErrorsOptionsPopUps = new string[3];
            _optionsSuccessOptionsPopUps = new int[3];
            //optiona
            _txtMessagesOptionsPopUps[0] =
                "La abuela tiene 400 pesos, decide comprar un peluche de 279 pesos. ¿Cuánto dinero le queda?";
            _txtMessagesButtonsOptionsPopUps[0] = "$ 111";
            _txtMessagesButtonsOptionsPopUps[1] = "$ 121";
            _txtMessagesButtonsOptionsPopUps[2] = "$ 131";
            _txtMessagesErrorsOptionsPopUps[0] =
                "400-279=121, a la abuela le quedan 121 pesos. ¡Sigamos con el segundo reto!";
            _optionsSuccessOptionsPopUps[0] = 1;
            _txtMessagesOptionsPopUps[1] =
                "La abuela tiene 400 pesos, decide comprar un peluche de 122 pesos. ¿Cuánto dinero le queda?";
            _txtMessagesButtonsOptionsPopUps[3] = "$ 278";
            _txtMessagesButtonsOptionsPopUps[4] = "$ 231";
            _txtMessagesButtonsOptionsPopUps[5] = "$ 279";
            _txtMessagesErrorsOptionsPopUps[1] =
                "400-122= 278, a la abuela le quedan 278 pesos. ¡Sigamos con el segundo reto!";
            _optionsSuccessOptionsPopUps[1] = 0;
            _txtMessagesOptionsPopUps[2] =
                "La abuela tiene 400 pesos, decide comprar un peluche de 167pesos. ¿Cuánto dinero le queda?";
            _txtMessagesButtonsOptionsPopUps[6] = "$ 233";
            _txtMessagesButtonsOptionsPopUps[7] = "$ 221";
            _txtMessagesButtonsOptionsPopUps[8] = "$ 133";
            _txtMessagesErrorsOptionsPopUps[2] =
                "400-167= 233, a la abuela le quedan 233 pesos. ¡Sigamos con el segundo reto!";
            _optionsSuccessOptionsPopUps[2] = 0;
            _stgMessagePopUpIndications[4] = "¡Correcto! ¡Sigamos con el segundo reto!";
        }

        //animaed string
        public void AnimatedString(float time, string t)
        {
            _text = t;
            _timeAnimation = time;
            _timeTotal = _timeAnimation;
            _animationStarted = true;
        }

        private void SetEnableBtnAceptNextActivity()
        {
            _btnAceptPopUpNextActivity.gameObject.SetActive(true);
        }

        //popup indications
        private void ShowPopUpIndications(int indexMessage)
        {
            PlaySoundOpenPopUp();
            _popUpIndications.SetActive(true);
            _txtMessagePopUpIndications.text = _stgMessagePopUpIndications[indexMessage];
        }

        //activity1
        private void ShowActivity1()
        {
            _valuesOccupied[0] = -1;
            _valuesOccupied[1] = -1;
            _valuesOccupied[2] = -1;
            _valuesOccupied[3] = -1;
            _arrayToysNamesAll[0] = "ImgSoccerBallPrice";
            _arrayToysNamesAll[1] = "ImgBookPrice";
            _arrayToysNamesAll[2] = "ImgDollHousePrice";
            _arrayToysNamesAll[3] = "ImgVideoGamePrice";
            _arrayToysNamesAll[4] = "ImgAirPlanePrice";
            _arrayToysNamesAll[5] = "ImgPianoPrice";
            _arrayToysNamesAll[6] = "ImgMoviePrice";
            _arrayToysNamesAll[7] = "ImgRobotPrice";
            _arrayToysNamesAll[8] = "ImgCarPrice";
            _arrayToysNumbersPricesAll[0] = 135;
            _arrayToysNumbersPricesAll[1] = 281;
            _arrayToysNumbersPricesAll[2] = 2798;
            _arrayToysNumbersPricesAll[3] = 1043;
            _arrayToysNumbersPricesAll[4] = 185;
            _arrayToysNumbersPricesAll[5] = 755;
            _arrayToysNumbersPricesAll[6] = 249;
            _arrayToysNumbersPricesAll[7] = 2998;
            _arrayToysNumbersPricesAll[8] = 1539;
            //get toys index
            var i = 0;
            while (i < 4)
            {
                var random = Random.Range(0, 9);
                if (CheckValuesRandoms(random)) continue;
                _valuesOccupied[i] = random;
                _arrayToysNumbersPricesGame[i] = _arrayToysNumbersPricesAll[random];
                i++;
            }
            //set image positions
            _imgPosition1 = _imgPosition1Toy.gameObject.transform.localPosition;
            _imgPosition1Label = _imgPosition1Price.gameObject.transform.localPosition;
            _imgPosition2 = _imgPosition2Toy.gameObject.transform.localPosition;
            _imgPosition2Label = _imgPosition2Price.gameObject.transform.localPosition;
            _imgPosition3 = _imgPosition3Toy.gameObject.transform.localPosition;
            _imgPosition3Label = _imgPosition3Price.gameObject.transform.localPosition;
            _imgPosition4 = _imgPosition4Toy.gameObject.transform.localPosition;
            _imgPosition4Label = _imgPosition4Price.gameObject.transform.localPosition;
            SetImagenPositionGame(_arrayToysNamesAll[_valuesOccupied[0]], 0);
            SetImagenPositionGame(_arrayToysNamesAll[_valuesOccupied[1]], 1);
            SetImagenPositionGame(_arrayToysNamesAll[_valuesOccupied[2]], 2);
            SetImagenPositionGame(_arrayToysNamesAll[_valuesOccupied[3]], 3);
            Quicksort(_arrayToysNumbersPricesGame, 0, 3);
            _nameImg1 = _arrayToysNamesAll[GetNameToys(_arrayToysNumbersPricesAll, _arrayToysNumbersPricesGame[0], 0)];
            _nameImg2 = _arrayToysNamesAll[GetNameToys(_arrayToysNumbersPricesAll, _arrayToysNumbersPricesGame[1], 0)];
            _nameImg3 = _arrayToysNamesAll[GetNameToys(_arrayToysNumbersPricesAll, _arrayToysNumbersPricesGame[2], 0)];
            _nameImg4 = _arrayToysNamesAll[GetNameToys(_arrayToysNumbersPricesAll, _arrayToysNumbersPricesGame[3], 0)];
        }

        //to check numbers in array without repeat
        private bool CheckValuesRandoms(int value)
        {
            for (var index = 0; index < 4; index++)
                if (value == _valuesOccupied[index])
                    return true;
            return false;
        }

        //to set toys to images
        private void SetImagenPositionGame(string imgName, int position)
        {
            switch (imgName)
            {
                case "ImgSoccerBallPrice":
                    switch (position)
                    {
                        case 0:
                            _imgSoccerBall.gameObject.transform.localPosition = _imgPosition1;
                            _imgSoccerBallPrice.gameObject.transform.localPosition = _imgPosition1Label;
                            break;
                        case 1:
                            _imgSoccerBall.gameObject.transform.localPosition = _imgPosition2;
                            _imgSoccerBallPrice.gameObject.transform.localPosition = _imgPosition2Label;
                            break;
                        case 2:
                            _imgSoccerBall.gameObject.transform.localPosition = _imgPosition3;
                            _imgSoccerBallPrice.gameObject.transform.localPosition = _imgPosition3Label;
                            break;
                        case 3:
                            _imgSoccerBall.gameObject.transform.localPosition = _imgPosition4;
                            _imgSoccerBallPrice.gameObject.transform.localPosition = _imgPosition4Label;
                            break;
                    }
                    break;
                case "ImgBookPrice":
                    switch (position)
                    {
                        case 0:
                            _imgBook.gameObject.transform.localPosition = _imgPosition1;
                            _imgBookPrice.gameObject.transform.localPosition = _imgPosition1Label;
                            break;
                        case 1:
                            _imgBook.gameObject.transform.localPosition = _imgPosition2;
                            _imgBookPrice.gameObject.transform.localPosition = _imgPosition2Label;
                            break;
                        case 2:
                            _imgBook.gameObject.transform.localPosition = _imgPosition3;
                            _imgBookPrice.gameObject.transform.localPosition = _imgPosition3Label;
                            break;
                        case 3:
                            _imgBook.gameObject.transform.localPosition = _imgPosition4;
                            _imgBookPrice.gameObject.transform.localPosition = _imgPosition4Label;
                            break;
                    }
                    break;
                case "ImgDollHousePrice":
                    switch (position)
                    {
                        case 0:
                            _imgDollHouse.gameObject.transform.localPosition = _imgPosition1;
                            _imgDollHousePrice.gameObject.transform.localPosition = _imgPosition1Label;
                            break;
                        case 1:
                            _imgDollHouse.gameObject.transform.localPosition = _imgPosition2;
                            _imgDollHousePrice.gameObject.transform.localPosition = _imgPosition2Label;
                            break;
                        case 2:
                            _imgDollHouse.gameObject.transform.localPosition = _imgPosition3;
                            _imgDollHousePrice.gameObject.transform.localPosition = _imgPosition3Label;
                            break;
                        case 3:
                            _imgDollHouse.gameObject.transform.localPosition = _imgPosition4;
                            _imgDollHousePrice.gameObject.transform.localPosition = _imgPosition4Label;
                            break;
                    }
                    break;
                case "ImgVideoGamePrice":
                    switch (position)
                    {
                        case 0:
                            _imgVideoGame.gameObject.transform.localPosition = _imgPosition1;
                            _imgVideoGamePrice.gameObject.transform.localPosition = _imgPosition1Label;
                            break;
                        case 1:
                            _imgVideoGame.gameObject.transform.localPosition = _imgPosition2;
                            _imgVideoGamePrice.gameObject.transform.localPosition = _imgPosition2Label;
                            break;
                        case 2:
                            _imgVideoGame.gameObject.transform.localPosition = _imgPosition3;
                            _imgVideoGamePrice.gameObject.transform.localPosition = _imgPosition3Label;
                            break;
                        case 3:
                            _imgVideoGame.gameObject.transform.localPosition = _imgPosition4;
                            _imgVideoGamePrice.gameObject.transform.localPosition = _imgPosition4Label;
                            break;
                    }
                    break;
                case "ImgAirPlanePrice":
                    switch (position)
                    {
                        case 0:
                            _imgAirPlane.gameObject.transform.localPosition = _imgPosition1;
                            _imgAirPlanePrice.gameObject.transform.localPosition = _imgPosition1Label;
                            break;
                        case 1:
                            _imgAirPlane.gameObject.transform.localPosition = _imgPosition2;
                            _imgAirPlanePrice.gameObject.transform.localPosition = _imgPosition2Label;
                            break;
                        case 2:
                            _imgAirPlane.gameObject.transform.localPosition = _imgPosition3;
                            _imgAirPlanePrice.gameObject.transform.localPosition = _imgPosition3Label;
                            break;
                        case 3:
                            _imgAirPlane.gameObject.transform.localPosition = _imgPosition4;
                            _imgAirPlanePrice.gameObject.transform.localPosition = _imgPosition4Label;
                            break;
                    }
                    break;
                case "ImgPianoPrice":
                    switch (position)
                    {
                        case 0:
                            _imgPiano.gameObject.transform.localPosition = _imgPosition1;
                            _imgPianoPrice.gameObject.transform.localPosition = _imgPosition1Label;
                            break;
                        case 1:
                            _imgPiano.gameObject.transform.localPosition = _imgPosition2;
                            _imgPianoPrice.gameObject.transform.localPosition = _imgPosition2Label;
                            break;
                        case 2:
                            _imgPiano.gameObject.transform.localPosition = _imgPosition3;
                            _imgPianoPrice.gameObject.transform.localPosition = _imgPosition3Label;
                            break;
                        case 3:
                            _imgPiano.gameObject.transform.localPosition = _imgPosition4;
                            _imgPianoPrice.gameObject.transform.localPosition = _imgPosition4Label;
                            break;
                    }
                    break;
                case "ImgMoviePrice":
                    switch (position)
                    {
                        case 0:
                            _imgMovie.gameObject.transform.localPosition = _imgPosition1;
                            _imgMoviePrice.gameObject.transform.localPosition = _imgPosition1Label;
                            break;
                        case 1:
                            _imgMovie.gameObject.transform.localPosition = _imgPosition2;
                            _imgMoviePrice.gameObject.transform.localPosition = _imgPosition2Label;
                            break;
                        case 2:
                            _imgMovie.gameObject.transform.localPosition = _imgPosition3;
                            _imgMoviePrice.gameObject.transform.localPosition = _imgPosition3Label;
                            break;
                        case 3:
                            _imgMovie.gameObject.transform.localPosition = _imgPosition4;
                            _imgMoviePrice.gameObject.transform.localPosition = _imgPosition4Label;
                            break;
                    }
                    break;
                case "ImgRobotPrice":
                    switch (position)
                    {
                        case 0:
                            _imgRobot.gameObject.transform.localPosition = _imgPosition1;
                            _imgRobotPrice.gameObject.transform.localPosition = _imgPosition1Label;
                            break;
                        case 1:
                            _imgRobot.gameObject.transform.localPosition = _imgPosition2;
                            _imgRobotPrice.gameObject.transform.localPosition = _imgPosition2Label;
                            break;
                        case 2:
                            _imgRobot.gameObject.transform.localPosition = _imgPosition3;
                            _imgRobotPrice.gameObject.transform.localPosition = _imgPosition3Label;
                            break;
                        case 3:
                            _imgRobot.gameObject.transform.localPosition = _imgPosition4;
                            _imgRobotPrice.gameObject.transform.localPosition = _imgPosition4Label;
                            break;
                    }
                    break;
                case "ImgCarPrice":
                    switch (position)
                    {
                        case 0:
                            _imgCar.gameObject.transform.localPosition = _imgPosition1;
                            _imgCarPrice.gameObject.transform.localPosition = _imgPosition1Label;
                            break;
                        case 1:
                            _imgCar.gameObject.transform.localPosition = _imgPosition2;
                            _imgCarPrice.gameObject.transform.localPosition = _imgPosition2Label;
                            break;
                        case 2:
                            _imgCar.gameObject.transform.localPosition = _imgPosition3;
                            _imgCarPrice.gameObject.transform.localPosition = _imgPosition3Label;
                            break;
                        case 3:
                            _imgCar.gameObject.transform.localPosition = _imgPosition4;
                            _imgCarPrice.gameObject.transform.localPosition = _imgPosition4Label;
                            break;
                    }
                    break;
            }
        }

        private static void Quicksort(int[] vector, int primero, int ultimo)
        {
            var central = (primero + ultimo)/2;
            double pivote = vector[central];
            var i = primero;
            var j = ultimo;
            do
            {
                while (vector[i] < pivote) i++;
                while (vector[j] > pivote) j--;
                if (i > j) continue;
                var temp = vector[i];
                vector[i] = vector[j];
                vector[j] = temp;
                i++;
                j--;
            } while (i <= j);
            if (primero < j)
                Quicksort(vector, primero, j);
            if (i < ultimo)
                Quicksort(vector, i, ultimo);
        }

        private static int GetNameToys(int[] array, int valueIndex, int option)
        {
            if (option == 0)
            {
                for (var index = 0; index < 9; index++)
                    if (array[index] == valueIndex)
                        return index;
            }
            else if (option == 1)
            {
                for (var index = 0; index < 12; index++)
                    if (array[index] == valueIndex)
                        return index;
            }
            return -1;
        }

        private void ShowPopUpError()
        {
            PlaySoundError();
            _popUpIndications.SetActive(true);
            _txtMessagePopUpIndications.text = _stgMessagePopUpIndications[1];
        }

        private void ShowPopUpSuccessAtivity1()
        {
            PlaySoundOpenPopUp();
            _popUpIndications.SetActive(true);
            _txtMessagePopUpIndications.text = _stgMessagePopUpIndications[2];
            _btnAceptPopUpIndications.onClick.RemoveAllListeners();
            _btnAceptPopUpIndications.onClick.AddListener(delegate
            {
                PlaySoundClosePopUp();
                _popUpIndications.SetActive(false);
                ShowPopUpActivity2();
            });
        }

        private void ShowPopUpActivity2()
        {
            PlaySoundOpenPopUp();
            _popUpIndications.SetActive(true);
            _txtMessagePopUpIndications.text = _stgMessagePopUpIndications[3];
            _btnAceptPopUpIndications.onClick.RemoveAllListeners();
            _btnAceptPopUpIndications.onClick.AddListener(delegate
            {
                PlaySoundClosePopUp();
                _popUpIndications.SetActive(false);
                _toys.SetActive(false);
                ShowActivity2();
            });
        }

        private void ShowActivity2()
        {
            _valuesOccupied[0] = -1;
            _valuesOccupied[1] = -1;
            _valuesOccupied[2] = -1;
            _valuesOccupied[3] = -1;
            _arrayTeddiesNamesAll[0] = "ImgTeddy1Price1";
            _arrayTeddiesNamesAll[1] = "ImgTeddy1Price2";
            _arrayTeddiesNamesAll[2] = "ImgTeddy1Price3";
            _arrayTeddiesNamesAll[3] = "ImgTeddy2Price1";
            _arrayTeddiesNamesAll[4] = "ImgTeddy2Price2";
            _arrayTeddiesNamesAll[5] = "ImgTeddy2Price3";
            _arrayTeddiesNamesAll[6] = "ImgTeddy3Price1";
            _arrayTeddiesNamesAll[7] = "ImgTeddy3Price2";
            _arrayTeddiesNamesAll[8] = "ImgTeddy3Price3";
            _arrayTeddiesNamesAll[9] = "ImgTeddy4Price1";
            _arrayTeddiesNamesAll[10] = "ImgTeddy4Price2";
            _arrayTeddiesNamesAll[11] = "ImgTeddy4Price3";
            //prices a
            _arrayTeddiesNumbersPricesAll[0] = 79;
            _arrayTeddiesNumbersPricesAll[1] = 167;
            _arrayTeddiesNumbersPricesAll[2] = 249;
            //prices b
            _arrayTeddiesNumbersPricesAll[3] = 80;
            _arrayTeddiesNumbersPricesAll[4] = 122;
            _arrayTeddiesNumbersPricesAll[5] = 139;
            //prices c
            _arrayTeddiesNumbersPricesAll[6] = 107;
            _arrayTeddiesNumbersPricesAll[7] = 149;
            _arrayTeddiesNumbersPricesAll[8] = 279;
            //prices d
            _arrayTeddiesNumbersPricesAll[9] = 98;
            _arrayTeddiesNumbersPricesAll[10] = 111;
            _arrayTeddiesNumbersPricesAll[11] = 150;
            //get teddies index
            _valuesOccupied[0] = Random.Range(0, 3);
            _valuesOccupied[1] = Random.Range(3, 6);
            _valuesOccupied[2] = Random.Range(6, 9);
            _valuesOccupied[3] = Random.Range(9, 12);
            _arrayTeddiesNumbersPricesGame[0] = _arrayTeddiesNumbersPricesAll[_valuesOccupied[0]];
            _arrayTeddiesNumbersPricesGame[1] = _arrayTeddiesNumbersPricesAll[_valuesOccupied[1]];
            _arrayTeddiesNumbersPricesGame[2] = _arrayTeddiesNumbersPricesAll[_valuesOccupied[2]];
            _arrayTeddiesNumbersPricesGame[3] = _arrayTeddiesNumbersPricesAll[_valuesOccupied[3]];
            //set image positions
            _imgPosition1 = _imgPosition1Toy.gameObject.transform.localPosition;
            _imgPosition1Label = _imgPosition1Price.gameObject.transform.localPosition;
            _imgPosition2 = _imgPosition2Toy.gameObject.transform.localPosition;
            _imgPosition2Label = _imgPosition2Price.gameObject.transform.localPosition;
            _imgPosition3 = _imgPosition3Toy.gameObject.transform.localPosition;
            _imgPosition3Label = _imgPosition3Price.gameObject.transform.localPosition;
            _imgPosition4 = _imgPosition4Toy.gameObject.transform.localPosition;
            _imgPosition4Label = _imgPosition4Price.gameObject.transform.localPosition;
            _imgTeddy1.gameObject.transform.localPosition = _imgPosition1;
            _imgTeddy2.gameObject.transform.localPosition = _imgPosition2;
            _imgTeddy3.gameObject.transform.localPosition = _imgPosition3;
            _imgTeddy4.gameObject.transform.localPosition = _imgPosition4;
            SetImagenPositionGameTeddies(_arrayTeddiesNamesAll[_valuesOccupied[0]]);
            SetImagenPositionGameTeddies(_arrayTeddiesNamesAll[_valuesOccupied[1]]);
            SetImagenPositionGameTeddies(_arrayTeddiesNamesAll[_valuesOccupied[2]]);
            SetImagenPositionGameTeddies(_arrayTeddiesNamesAll[_valuesOccupied[3]]);
            Quicksort(_arrayTeddiesNumbersPricesGame, 0, 3);
            _nameImg1 =
                _arrayTeddiesNamesAll[GetNameToys(_arrayTeddiesNumbersPricesAll, _arrayTeddiesNumbersPricesGame[0], 1)];
            _nameImg2 =
                _arrayTeddiesNamesAll[GetNameToys(_arrayTeddiesNumbersPricesAll, _arrayTeddiesNumbersPricesGame[1], 1)];
            _nameImg3 =
                _arrayTeddiesNamesAll[GetNameToys(_arrayTeddiesNumbersPricesAll, _arrayTeddiesNumbersPricesGame[2], 1)];
            _nameImg4 =
                _arrayTeddiesNamesAll[GetNameToys(_arrayTeddiesNumbersPricesAll, _arrayTeddiesNumbersPricesGame[3], 1)];
            _draggingTeddy = false;
            _btnAceptPopUpIndications.onClick.RemoveAllListeners();
            _btnAceptPopUpIndications.onClick.AddListener(delegate
            {
                PlaySoundClosePopUp();
                _popUpIndications.SetActive(false);
            });
            PlaySoundOpenPopUp();
        }

        private void SetImagenPositionGameTeddies(string imgName)
        {
            switch (imgName)
            {
                case "ImgTeddy1Price1":
                    _imgTeddy1Price1.gameObject.transform.localPosition = _imgPosition1Label;
                    break;
                case "ImgTeddy1Price2":
                    _imgTeddy1Price2.gameObject.transform.localPosition = _imgPosition1Label;
                    break;
                case "ImgTeddy1Price3":
                    _imgTeddy1Price3.gameObject.transform.localPosition = _imgPosition1Label;
                    break;
                case "ImgTeddy2Price1":
                    _imgTeddy2Price1.gameObject.transform.localPosition = _imgPosition2Label;
                    break;
                case "ImgTeddy2Price2":
                    _imgTeddy2Price2.gameObject.transform.localPosition = _imgPosition2Label;
                    break;
                case "ImgTeddy2Price3":
                    _imgTeddy2Price3.gameObject.transform.localPosition = _imgPosition2Label;
                    break;
                case "ImgTeddy3Price1":
                    _imgTeddy3Price1.gameObject.transform.localPosition = _imgPosition3Label;
                    break;
                case "ImgTeddy3Price2":
                    _imgTeddy3Price2.gameObject.transform.localPosition = _imgPosition3Label;
                    break;
                case "ImgTeddy3Price3":
                    _imgTeddy3Price3.gameObject.transform.localPosition = _imgPosition3Label;
                    break;
                case "ImgTeddy4Price1":
                    _imgTeddy4Price1.gameObject.transform.localPosition = _imgPosition4Label;
                    break;
                case "ImgTeddy4Price2":
                    _imgTeddy4Price2.gameObject.transform.localPosition = _imgPosition4Label;
                    break;
                case "ImgTeddy4Price3":
                    _imgTeddy4Price3.gameObject.transform.localPosition = _imgPosition4Label;
                    break;
            }
        }

        private void PopUpsOptionsQuestions()
        {
            PlaySoundOpenPopUp();
            _optionsPopUps.SetActive(true);
            _indexTxtMessagesOptionsPopUps = Random.Range(0, 3);
            _txtMessageCurrentOptionsPopUps.text = _txtMessagesOptionsPopUps[_indexTxtMessagesOptionsPopUps];
            switch (_indexTxtMessagesOptionsPopUps)
            {
                case 0:
                    _btnAnswerA.transform.FindChild("TxtAnswer").GetComponent<Text>().text =
                        _txtMessagesButtonsOptionsPopUps[0];
                    _btnAnswerB.transform.FindChild("TxtAnswer").GetComponent<Text>().text =
                        _txtMessagesButtonsOptionsPopUps[1];
                    _btnAnswerC.transform.FindChild("TxtAnswer").GetComponent<Text>().text =
                        _txtMessagesButtonsOptionsPopUps[2];
                    break;
                case 1:
                    _btnAnswerA.transform.FindChild("TxtAnswer").GetComponent<Text>().text =
                        _txtMessagesButtonsOptionsPopUps[3];
                    _btnAnswerB.transform.FindChild("TxtAnswer").GetComponent<Text>().text =
                        _txtMessagesButtonsOptionsPopUps[4];
                    _btnAnswerC.transform.FindChild("TxtAnswer").GetComponent<Text>().text =
                        _txtMessagesButtonsOptionsPopUps[5];
                    break;
                case 2:
                    _btnAnswerA.transform.FindChild("TxtAnswer").GetComponent<Text>().text =
                        _txtMessagesButtonsOptionsPopUps[6];
                    _btnAnswerB.transform.FindChild("TxtAnswer").GetComponent<Text>().text =
                        _txtMessagesButtonsOptionsPopUps[7];
                    _btnAnswerC.transform.FindChild("TxtAnswer").GetComponent<Text>().text =
                        _txtMessagesButtonsOptionsPopUps[8];
                    break;
            }
        }

        private void UpdateInfoPopUp(int option)
        {
            _btnAceptPopUpIndications.onClick.RemoveAllListeners();
            _btnAceptPopUpIndications.onClick.AddListener(delegate
            {
                PlaySoundClosePopUp();
                _popUpIndications.SetActive(false);
                _challenge1.SetActive(false);
                PlaySoundEndChallenge();
                _challenge2.SetActive(true);
            });
            PlaySoundClosePopUp();
            _optionsPopUps.SetActive(false);
            if (option != _optionsSuccessOptionsPopUps[_indexTxtMessagesOptionsPopUps])
            {
                //fails
                PlaySoundOpenPopUp();
                _popUpIndications.SetActive(true);
                PlaySoundError();
                _txtMessagePopUpIndications.text = _txtMessagesErrorsOptionsPopUps[_indexTxtMessagesOptionsPopUps];
            }
            else //success
            {
                PlaySoundOpenPopUp();
                _popUpIndications.SetActive(true);
                PlaySoundSuccess();
                _txtMessagePopUpIndications.text = _stgMessagePopUpIndications[4];
            }
        }

        private void PlaySoundSuccess()
        {
            _soundManager.PlaySound("Sound_success", 1);
        }

        private void PlaySoundError()
        {
            _soundManager.PlaySound("Sound_error", 2);
        }

        private void PlaySoundOpenPopUp()
        {
            _soundManager.PlaySound("Sound_openPopUp", 3);
        }

        private void PlaySoundClosePopUp()
        {
            _soundManager.PlaySound("Sound_closePopUp", 4);
        }

        private void PlaySoundEndChallenge()
        {
            _soundManager.PlaySound("Sound_endChallenge", 2);
        }
    }
}