﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.Resources.Scripts
{
    public class Reto3Oda12 : MonoBehaviour
    {
        private Animator _animationOption1;
        private Animator _animationOption2;
        private Animator _animationOption3;
        private bool _animationStarted;
        private bool _ban;
        private Button _btnAceptFinal;
        private Button _btnAceptNextActivity;
        private Button _btnAnswerA;
        private Button _btnAnswerB;
        private Button _btnAnswerC;
        private Button _btnClicNextActivity;
        private Button _btnErrorAbc;
        private GameObject _candies;
        private GameObject _challenge3;
        private GameObject _challenge4;
        private int _indexTxtMessages;
        private int _indexTxtMessagesButtons;
        private string _messagePopUpNextActivity;
        private string _nameStarsVar;
        private GameObject _optionAPopUp;
        private GameObject _optionBPopUp;
        private GameObject _optionCPopUp;
        private GameObject _optionsPopUps;
        private GameObject _optionsPopUpsError;
        private GameObject _optionsPopUpsSuccess;
        private int[] _optionsSuccess;
        private int[] _optionsSuccessA;
        private int[] _optionsSuccessB;
        private int[] _optionsSuccessC;
        private GameObject _popUpFinal;
        private GameObject _popUpNextActivity;
        private int _randomIndexOptionAbc;
        private GameObject _score;
        private GameObject _screen12;
        private SoundManager _soundManager;
        private int _starCountGame;
        private string _text;
        private float _timeAnimation;
        private float _timeAnimationPopUpNextActivity;
        private float _timeAnimations;
        private float _timeTotal;
        private Text _txtAnswerA;
        private Text _txtAnswerB;
        private Text _txtAnswerC;
        private Text _txtErrorAbc;
        private Text _txtMessage;
        private Text _txtMessagePopUpFinal;
        private Text _txtMessagePopUpNextActivity;
        private string[] _txtMessagesActivity;
        private string[] _txtMessagesActivityA;
        private string[] _txtMessagesActivityB;
        private string[] _txtMessagesActivityC;
        private string[] _txtMessagesButtonsActivity;
        private string[] _txtMessagesButtonsActivityA;
        private string[] _txtMessagesButtonsActivityB;
        private string[] _txtMessagesButtonsActivityC;
        private string[] _txtMessagesErrorsActivity;
        private string[] _txtMessagesErrorsActivityA;
        private string[] _txtMessagesErrorsActivityB;
        private string[] _txtMessagesErrorsActivityC;
        private Text _txtScore;

        internal void Start()
        {
            _soundManager = transform.GetComponent<SoundManager>();
            //get main node
            _challenge3 = transform.GetChild(4).gameObject;
            _score = _challenge3.transform.GetChild(0).FindChild("Score").gameObject;
            _txtScore = _score.transform.FindChild("TxtScore").GetComponent<Text>();
            //get node for popup 1 with actions and elements
            _messagePopUpNextActivity =
                "¿Me ayudarías a preparar los bolos?";
            _popUpNextActivity = _challenge3.transform.GetChild(0).FindChild("PopUpNextActivity").gameObject;
            _txtMessagePopUpNextActivity =
                _popUpNextActivity.transform.FindChild("PopUpImage")
                    .transform.FindChild("TxtMessage")
                    .GetComponent<Text>();

            _btnAceptNextActivity = _popUpNextActivity.transform.FindChild("BtnAcept").GetComponent<Button>();
            _btnAceptNextActivity.onClick.AddListener(delegate
            {
                PlaySoundClosePopUp();
                _popUpNextActivity.SetActive(false);
                _screen12.SetActive(true);
                _candies.SetActive(true);
                ShowOptionsPopUps(_indexTxtMessages, _indexTxtMessagesButtons);
            });
            _btnClicNextActivity = _popUpNextActivity.transform.FindChild("PopUpImage").GetComponent<Button>();
            _btnClicNextActivity.onClick.AddListener(delegate
            {
                if (_ban) return;
                AnimatedString(_timeAnimationPopUpNextActivity, _messagePopUpNextActivity);
                Invoke("SetActiveBtnAceptNextActivity", _timeAnimationPopUpNextActivity + 0.5f);
                _ban = true;
                _score.SetActive(true);
                _starCountGame = PlayerPrefs.GetInt(_nameStarsVar);
                _txtScore.text = _starCountGame.ToString();
            });
            //nodes for options
            _screen12 = _challenge3.transform.GetChild(0).FindChild("Screen12").gameObject;
            _candies = _screen12.transform.FindChild("Candies").gameObject;
            _candies.SetActive(true);
            _optionsPopUps = _screen12.transform.FindChild("OptionsPopUps").gameObject;
            _txtMessage = _optionsPopUps.transform.FindChild("TxtMessage").GetComponent<Text>();
            _btnAnswerA = _optionsPopUps.transform.FindChild("BtnAnswerA").GetComponent<Button>();
            _btnAnswerA.onClick.AddListener(delegate { UpdateInfoPopUp(0); });
            _btnAnswerB = _optionsPopUps.transform.FindChild("BtnAnswerB").GetComponent<Button>();
            _btnAnswerB.onClick.AddListener(delegate { UpdateInfoPopUp(1); });
            _btnAnswerC = _optionsPopUps.transform.FindChild("BtnAnswerC").GetComponent<Button>();
            _btnAnswerC.onClick.AddListener(delegate { UpdateInfoPopUp(2); });
            _txtAnswerA = _btnAnswerA.transform.FindChild("TxtAnswer").GetComponent<Text>();
            _txtAnswerB = _btnAnswerB.transform.FindChild("TxtAnswer").GetComponent<Text>();
            _txtAnswerC = _btnAnswerC.transform.FindChild("TxtAnswer").GetComponent<Text>();
            _optionsPopUpsSuccess = _screen12.transform.FindChild("OptionsPopUpsSuccess").gameObject;
            _optionsPopUpsError = _screen12.transform.FindChild("OptionsPopUpErrors").gameObject;
            _btnErrorAbc = _optionsPopUpsError.transform.FindChild("BtnAcept").GetComponent<Button>();
            _btnErrorAbc.onClick.AddListener(delegate
            {
                PlaySoundClosePopUp();
                _optionsPopUpsError.SetActive(false);
                ShowAnimations();
            });
            _txtErrorAbc = _optionsPopUpsError.transform.FindChild("TxtMessage").GetComponent<Text>();
            _challenge4 = transform.GetChild(5).gameObject;
            _popUpFinal = _challenge3.transform.GetChild(0).FindChild("PopUpFinal").gameObject;
            _txtMessagePopUpFinal =
                _popUpFinal.transform.FindChild("PopUpImage")
                    .gameObject.transform.FindChild("TxtMessage")
                    .GetComponent<Text>();
            _btnAceptFinal = _popUpFinal.transform.FindChild("BtnAcept").GetComponent<Button>();
            _btnAceptFinal.onClick.AddListener(delegate
            {
                _challenge3.SetActive(false);
                PlaySoundEndChallenge();
                _challenge4.SetActive(true);
            });
            _nameStarsVar = "estrellasODA12_ODA12";
            _timeAnimationPopUpNextActivity = 5.0f;
            _timeAnimations = 5.0f;
            ShowPopUpNextActicity();
            SetUpValuesOptionsAbc();
            SetUpGameValues();
            ShowOptionsPopUps(_indexTxtMessages, _indexTxtMessagesButtons);
        }

        // Update is called once per frame
        internal void Update()
        {
            //siempre al final de la animacion
            if (!_animationStarted) return;
            _timeAnimation -= Time.deltaTime;
            if (_timeAnimation <= 0)
                _timeAnimation = 0;
            _txtMessagePopUpNextActivity.text = _text.Substring(0,
                (int) ((_timeTotal - _timeAnimation)*_text.Length/_timeTotal));
            _txtScore.text = _starCountGame.ToString();
        }

        public void AnimatedString(float time, string t)
        {
            _text = t;
            _timeAnimation = time;
            _timeTotal = _timeAnimation;
            _animationStarted = true;
        }

        private void ShowPopUpNextActicity()
        {
            _popUpNextActivity.SetActive(true);
        }

        private void SetActiveBtnAceptNextActivity()
        {
            _btnAceptNextActivity.transform.gameObject.SetActive(true);
        }

        private void SetUpValuesOptionsAbc()
        {
            // set arrays strings for messages in activity popups As 
            _txtMessagesActivityA = new string[3];
            _txtMessagesButtonsActivityA = new string[9];
            _txtMessagesErrorsActivityA = new string[3];
            _optionsSuccessA = new int[3];
            // values for activity popup As1 
            _txtMessagesActivityA[0] =
                "La abuela tiene 60 dulces y necesita preparar 5 bolos. Para que les toque la misma cantidad de dulces a cada uno, ¿cuántos dulces puede poner en cada bolo?";
            _txtMessagesButtonsActivityA[0] = "8";
            _txtMessagesButtonsActivityA[1] = "10";
            _txtMessagesButtonsActivityA[2] = "12";
            _txtMessagesErrorsActivityA[0] =
                "Hay que repartir 60 dulces entre 5 bolsitas:  60/5= 12. Cada bolo debe contener 12 dulces.";
            _optionsSuccessA[0] = 2;
            // values for activity popup As2
            _txtMessagesActivityA[1] =
                "Carlos acaba de invitar a Valeria, la hija de la vecina. La abuela tiene que volver a repartir los 60 dulces entre 6 bolos,  ¿cuántos dulces puede poner en cada bolo?";
            _txtMessagesButtonsActivityA[3] = "6";
            _txtMessagesButtonsActivityA[4] = "10";
            _txtMessagesButtonsActivityA[5] = "12";
            _txtMessagesErrorsActivityA[1] =
                "Hay que repartir 60 dulces entre 6 bolsitas:  60/6= 10. Cada bolo debe contener 10 dulces.";
            _optionsSuccessA[1] = 1;
            // values for activity popup As2
            _txtMessagesActivityA[2] =
                "La abuela se comió 6 dulces mientras preparaba los bolos, ahora tiene que repartir 54 dulces entre 6 bolos,  ¿cuántos dulces puede poner en cada bolo?";
            _txtMessagesButtonsActivityA[6] = "3";
            _txtMessagesButtonsActivityA[7] = "6";
            _txtMessagesButtonsActivityA[8] = "9";
            _txtMessagesErrorsActivityA[2] =
                "Hay que repartir 54 dulces entre 6 bolsitas:  54/6=9. Cada bolo debe contener 9 dulces.";
            _optionsSuccessA[2] = 2;
            // set arrays strings for messages in activity popups Bs
            _txtMessagesActivityB = new string[3];
            _txtMessagesButtonsActivityB = new string[9];
            _txtMessagesErrorsActivityB = new string[3];
            _optionsSuccessB = new int[3];
            // values for activity popup Bs1
            _txtMessagesActivityB[0] =
                "La abuela tiene 40 dulces y necesita preparar 4 bolos. Para que les toque la misma cantidad de dulces a cada uno, ¿cuántos dulces puede poner en cada bolo?";
            _txtMessagesButtonsActivityB[0] = "10";
            _txtMessagesButtonsActivityB[1] = "12";
            _txtMessagesButtonsActivityB[2] = "13";
            _txtMessagesErrorsActivityB[0] =
                "Hay que repartir 40 dulces entre 4 bolsitas:  40/4= 10. Cada bolo debe contener 10 dulces.";
            _optionsSuccessB[0] = 0;
            // values for activity popup As2
            _txtMessagesActivityB[1] =
                "Carlos acaba de invitar a Valeria, la hija de la vecina. La abuela tiene que volver a repartir los 40 dulces entre 5 bolos,  ¿cuántos dulces puede poner en cada bolo?";
            _txtMessagesButtonsActivityB[3] = "5";
            _txtMessagesButtonsActivityB[4] = "8";
            _txtMessagesButtonsActivityB[5] = "10";
            _txtMessagesErrorsActivityB[1] =
                "Hay que repartir 40 dulces entre 5 bolsitas:  40/5= 8. Cada bolo debe contener 8 dulces.";
            _optionsSuccessB[1] = 1;
            // values for activity popup As2
            _txtMessagesActivityB[2] =
                "La abuela se comió 5 dulces mientras preparaba los bolos, ahora tiene que repartir 35 dulces entre 5 bolos,  ¿cuántos dulces puede poner en cada bolo?";
            _txtMessagesButtonsActivityB[6] = "5";
            _txtMessagesButtonsActivityB[7] = "6";
            _txtMessagesButtonsActivityB[8] = "7";
            _txtMessagesErrorsActivityB[2] =
                "Hay que repartir 35 dulces entre 5 bolsitas:  35/5=7. Cada bolo debe contener 7 dulces.";
            _optionsSuccessB[2] = 2;
            // set arrays strings for messages in activity popups Cs
            _txtMessagesActivityC = new string[3];
            _txtMessagesButtonsActivityC = new string[9];
            _txtMessagesErrorsActivityC = new string[3];
            _optionsSuccessC = new int[3];
            // values for activity popup As3
            _txtMessagesActivityC[0] =
                "La abuela tiene 33 dulces y necesita preparar 3 bolos. Para que les toque la misma cantidad de dulces a cada uno, ¿cuántos dulces puede poner en cada bolo?";
            _txtMessagesButtonsActivityC[0] = "10";
            _txtMessagesButtonsActivityC[1] = "11";
            _txtMessagesButtonsActivityC[2] = "13";
            _txtMessagesErrorsActivityC[0] =
                "Hay que repartir 33 dulces entre 3 bolsitas:  33/3= 11. Cada bolo debe contener 11 dulces.";
            _optionsSuccessC[0] = 1;
            // values for activity popup As2
            _txtMessagesActivityC[1] =
                "Carlos acaba de invitar a Valeria y la abuela se comió 1 dulce mientras preparaba los bolos, quedan 32 dulces a repartir entre 4 bolos, ¿cuántos dulces puede poner en cada bolo?";
            _txtMessagesButtonsActivityC[3] = "6";
            _txtMessagesButtonsActivityC[4] = "7";
            _txtMessagesButtonsActivityC[5] = "8";
            _txtMessagesErrorsActivityC[1] =
                "Hay que repartir 32 dulces entre 4 bolsitas:  32/4=8. Cada bolo debe contener 8 dulces.";
            _optionsSuccessC[1] = 2;
            // values for activity popup As2
            _txtMessagesActivityC[2] =
                "La abuela se comió 2 dulces más y llega un invitado sorpresa, ahora tiene que repartir 30 dulces entre 5 bolos,  ¿cuántos dulces puede poner en cada bolo?";
            _txtMessagesButtonsActivityC[6] = "5";
            _txtMessagesButtonsActivityC[7] = "6";
            _txtMessagesButtonsActivityC[8] = "7";
            _txtMessagesErrorsActivityC[2] =
                "Hay que repartir 30 dulces entre 5 bolsitas:  30/5=6. Cada bolo debe contener 6 dulces.";
            _optionsSuccessC[2] = 1;
            // set arrays strings for messages in activity popups Cs
            _txtMessagesActivity = new string[3];
            _txtMessagesButtonsActivity = new string[9];
            _txtMessagesErrorsActivity = new string[3];
            _optionsSuccess = new int[3];
            _txtMessagePopUpFinal.text = "Ya están listos los bolos, ¡sigamos con el cuarto reto!";
        }

        private void SetUpGameValues()
        {
            _randomIndexOptionAbc = Random.Range(0, 3);
            switch (_randomIndexOptionAbc)
            {
                case 0:
                    _txtMessagesActivity = _txtMessagesActivityA;
                    _txtMessagesButtonsActivity = _txtMessagesButtonsActivityA;
                    _txtMessagesErrorsActivity = _txtMessagesErrorsActivityA;
                    _optionsSuccess = _optionsSuccessA;
                    _optionAPopUp =
                        _optionsPopUpsSuccess.transform.FindChild("OptionsAs")
                            .gameObject.transform.FindChild("OptionA1")
                            .gameObject;
                    _optionBPopUp =
                        _optionsPopUpsSuccess.transform.FindChild("OptionsAs")
                            .gameObject.transform.FindChild("OptionA2")
                            .gameObject;
                    _optionCPopUp =
                        _optionsPopUpsSuccess.transform.FindChild("OptionsAs")
                            .gameObject.transform.FindChild("OptionA3")
                            .gameObject;
                    _animationOption1 = _optionAPopUp.transform.FindChild("Candies").transform.GetComponent<Animator>();
                    _animationOption2 = _optionBPopUp.transform.FindChild("Candies").transform.GetComponent<Animator>();
                    _animationOption3 = _optionCPopUp.transform.FindChild("Candies").transform.GetComponent<Animator>();
                    break;
                case 1:
                    _txtMessagesActivity = _txtMessagesActivityB;
                    _txtMessagesButtonsActivity = _txtMessagesButtonsActivityB;
                    _txtMessagesErrorsActivity = _txtMessagesErrorsActivityB;
                    _optionsSuccess = _optionsSuccessB;
                    _optionAPopUp =
                        _optionsPopUpsSuccess.transform.FindChild("OptionsBs")
                            .gameObject.transform.FindChild("OptionB1")
                            .gameObject;
                    _optionBPopUp =
                        _optionsPopUpsSuccess.transform.FindChild("OptionsBs")
                            .gameObject.transform.FindChild("OptionB2")
                            .gameObject;
                    _optionCPopUp =
                        _optionsPopUpsSuccess.transform.FindChild("OptionsBs")
                            .gameObject.transform.FindChild("OptionB3")
                            .gameObject;
                    _animationOption1 = _optionAPopUp.transform.FindChild("Candies").transform.GetComponent<Animator>();
                    _animationOption2 = _optionBPopUp.transform.FindChild("Candies").transform.GetComponent<Animator>();
                    _animationOption3 = _optionCPopUp.transform.FindChild("Candies").transform.GetComponent<Animator>();
                    break;
                case 2:
                    _txtMessagesActivity = _txtMessagesActivityC;
                    _txtMessagesButtonsActivity = _txtMessagesButtonsActivityC;
                    _txtMessagesErrorsActivity = _txtMessagesErrorsActivityC;
                    _optionsSuccess = _optionsSuccessC;
                    _optionAPopUp =
                        _optionsPopUpsSuccess.transform.FindChild("OptionsCs")
                            .gameObject.transform.FindChild("OptionC1")
                            .gameObject;
                    _optionBPopUp =
                        _optionsPopUpsSuccess.transform.FindChild("OptionsCs")
                            .gameObject.transform.FindChild("OptionC2")
                            .gameObject;
                    _optionCPopUp =
                        _optionsPopUpsSuccess.transform.FindChild("OptionsCs")
                            .gameObject.transform.FindChild("OptionC3")
                            .gameObject;
                    _animationOption1 = _optionAPopUp.transform.FindChild("Candies").transform.GetComponent<Animator>();
                    _animationOption2 = _optionBPopUp.transform.FindChild("Candies").transform.GetComponent<Animator>();
                    _animationOption3 = _optionCPopUp.transform.FindChild("Candies").transform.GetComponent<Animator>();
                    break;
            }
        }

        private void ShowOptionsPopUps(int indexMessages, int indexMessagesButtons)
        {
            if ((indexMessages >= 3) || (indexMessagesButtons >= 9)) return;
            PlaySoundClosePopUp();
            _candies.SetActive(true);
            _optionAPopUp.SetActive(false);
            _optionBPopUp.SetActive(false);
            _optionCPopUp.SetActive(false);
            _optionsPopUpsSuccess.SetActive(false);
            _optionsPopUpsError.SetActive(false);
            PlaySoundOpenPopUp();
            _optionsPopUps.SetActive(true);
            _txtMessage.text = _txtMessagesActivity[indexMessages];
            _txtAnswerA.text = _txtMessagesButtonsActivity[indexMessagesButtons];
            _txtAnswerB.text = _txtMessagesButtonsActivity[indexMessagesButtons + 1];
            _txtAnswerC.text = _txtMessagesButtonsActivity[indexMessagesButtons + 2];
            _txtErrorAbc.text = _txtMessagesErrorsActivity[indexMessages];
        }

        private void UpdateInfoPopUp(int option)
        {
            PlaySoundClosePopUp();
            _optionsPopUps.SetActive(false);
            if (option != _optionsSuccess[_indexTxtMessages])
            {
                PlaySoundOpenPopUp();
                _optionsPopUpsError.SetActive(true);
                PlaySoundError();
            }
            else
            {
                PlaySoundSuccess();
                _starCountGame += 2;
                PlayerPrefs.SetInt(_nameStarsVar, _starCountGame);
                ShowAnimations();
            }
        }

        private void ShowAnimations()
        {
            PlaySoundOpenPopUp();
            _optionsPopUpsSuccess.SetActive(true);
            CheckCurrentAnimation(_indexTxtMessages);
            _candies.SetActive(false);
            StartCoroutine(Wait());
        }

        private void ShowPopUpFinal()
        {
            PlaySoundClosePopUp();
            _screen12.SetActive(false);
            _popUpFinal.SetActive(true);
        }

        private IEnumerator Wait()
        {
            yield return new WaitForSeconds(_timeAnimations);
            ShowOptionsPopUps(_indexTxtMessages += 1, _indexTxtMessagesButtons += 3);
            if (_indexTxtMessages >= 3)
                ShowPopUpFinal();
        }

        private void CheckCurrentAnimation(int indexCurrentAnswer)
        {
            PlaySoundOpenPopUp();
            switch (_randomIndexOptionAbc)
            {
                case 0:
                    switch (indexCurrentAnswer)
                    {
                        case 0:
                            _optionAPopUp.SetActive(true);
                            _animationOption1.Play("MovementsA1");
                            break;
                        case 1:
                            _optionBPopUp.SetActive(true);
                            _animationOption2.Play("MovementsA2");
                            break;
                        case 2:
                            _optionCPopUp.SetActive(true);
                            _animationOption3.Play("MovementsA3");
                            break;
                    }
                    break;
                case 1:
                    switch (indexCurrentAnswer)
                    {
                        case 0:
                            _optionAPopUp.SetActive(true);
                            _animationOption1.Play("MovementsB1");
                            break;
                        case 1:
                            _optionBPopUp.SetActive(true);
                            _animationOption2.Play("MovementsB2");
                            break;
                        case 2:
                            _optionCPopUp.SetActive(true);
                            _animationOption3.Play("MovementsB3");
                            break;
                    }
                    break;
                case 2:
                    switch (indexCurrentAnswer)
                    {
                        case 0:
                            _optionAPopUp.SetActive(true);
                            _animationOption1.Play("MovementsC1");
                            break;
                        case 1:
                            _optionBPopUp.SetActive(true);
                            _animationOption2.Play("MovementsC2");
                            break;
                        case 2:
                            _optionCPopUp.SetActive(true);
                            _animationOption3.Play("MovementsC3");
                            break;
                    }
                    break;
            }
        }

        private void PlaySoundSuccess()
        {
            _soundManager.PlaySound("Sound_success", 1);
        }

        private void PlaySoundError()
        {
            _soundManager.PlaySound("Sound_error", 2);
        }

        private void PlaySoundOpenPopUp()
        {
            _soundManager.PlaySound("Sound_openPopUp", 3);
        }

        private void PlaySoundClosePopUp()
        {
            _soundManager.PlaySound("Sound_closePopUp", 4);
        }

        private void PlaySoundEndChallenge()
        {
            _soundManager.PlaySound("Sound_endChallenge", 2);
        }
    }
}