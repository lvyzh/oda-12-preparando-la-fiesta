﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.Resources.Scripts
{
    public class Reto2Oda12 : MonoBehaviour
    {
        private bool _animationStarted;
        private int _banActiveOptionsPackages;
        private bool _banPopUpNextActivity;
        private bool _banPopUpNextActivity2;
        private Button _btnAceptPopUpError;
        private Button _btnAceptPopUpErrorPackage;
        private Button _btnAceptPopUpIndications;
        private Button _btnAceptPopUpNextActivity;
        private Button _btnAceptPopUpNextActivity2;
        private Button _btnAnswerA;
        private Button _btnAnswerAPackages;
        private Button _btnAnswerB;
        private Button _btnAnswerBPackages;
        private Button _btnAnswerC;
        private Button _btnAnswerCPackages;
        private Button _btnGlobePopUpNextActivity;
        private Button _btnGlobePopUpNextActivity2;
        private GameObject _challenge2;
        private GameObject _challenge3;
        private int _indexMessageOptionsPopUpsDisplay;
        private int _indexMessagePackagesDisplay;
        private int _indexMessagesOptionsPopUpsAux;
        private int _indexMessagesPackagesAux;
        private int _indexTxtMessagesButtonsMax;
        private int _indexTxtMessagesButtonsMin;
        private int _indexTxtMessagesOptionsPopUps;
        private GameObject _imgPackage1;
        private GameObject _imgPackage2;
        private GameObject _imgPackage3;
        private GameObject _imgPackage4;
        //private static string "estrellasODA12_ODA12" = "estrellasODA12_ODA12";
        private GameObject _optionsPopUps;
        private GameObject _optionsPopUpsPackage;
        private int[] _optionsSuccessOptionsPopUps;
        private int[] _optionsSuccessPackages;
        private Button _pinataHorse;
        private Button _pinataSpecial;
        private Button _pinataTradicional;
        private GameObject _popUpError;
        private GameObject _popUpErrorPackage;
        private GameObject _popUpIndications;
        private GameObject _popUpNextActivity;
        private GameObject _popUpNextActivity2;
        private GameObject _score;
        private GameObject _screen9;
        private SoundManager _soundManager;
        private int _starCountGame;
        private string[] _stgMessagePopUpIndications;
        private string _stgMessagePopUpNextActivity;
        private string _stgMessagePopUpNextActivity2;
        private string _text;
        private float _timeAnimation;
        private float _timeTotal;
        private float _timeTxtMessagePopUpNextActivity;
        private float _timeTxtMessagePopUpNextActivity2;
        private Text _txtMessageCurrentOptionsPopUps;
        private Text _txtMessageCurrentPackages;
        private Text _txtMessagePopUpError;
        private Text _txtMessagePopUpErrorPackage;
        private Text _txtMessagePopUpIndications;
        private Text _txtMessagePopUpNextActivity;
        private Text _txtMessagePopUpNextActivity2;
        private string[] _txtMessagesButtonsOptionsPopUps;
        private string[] _txtMessagesButtonsPackages;
        private string[] _txtMessagesErrorsOptionsPopUps;
        private string[] _txtMessagesErrorsPackages;
        private string[] _txtMessagesOptionsPopUps;
        private string[] _txtMessagesPackages;
        private Text _txtScore;

        internal void Start()
        {
            SetUpValues();
            _soundManager = transform.GetComponent<SoundManager>();
            _starCountGame = PlayerPrefs.GetInt("estrellasODA12_ODA12");
            _challenge2 = transform.GetChild(3).gameObject;
            _challenge3 = transform.GetChild(4).gameObject;
            //get elements stars
            _score = _challenge2.transform.GetChild(0).FindChild("Score").gameObject;
            _txtScore = _score.transform.FindChild("TxtScore").GetComponent<Text>();
            
            //popUpNextActivity
            _popUpNextActivity = _challenge2.transform.GetChild(0).FindChild("PopUpNextActivity").gameObject;
            _btnGlobePopUpNextActivity =
                _popUpNextActivity.transform.FindChild("PopUp").transform.FindChild("PopUpImage").GetComponent<Button>();
            _btnGlobePopUpNextActivity.onClick.AddListener(delegate
            {
                _score.SetActive(true);
                _starCountGame = PlayerPrefs.GetInt("estrellasODA12_ODA12");
                _txtScore.text = _starCountGame.ToString();
                ShowTextPopUpNextActivity();
            });
            _btnAceptPopUpNextActivity =
                _popUpNextActivity.transform.FindChild("PopUp").transform.FindChild("BtnAcept").GetComponent<Button>();
            _btnAceptPopUpNextActivity.onClick.AddListener(delegate
            {
                PlaySoundClosePopUp();
                _popUpNextActivity.SetActive(false);
                _txtMessagePopUpNextActivity.text = string.Empty;
                ShowPopUpIndications(0);
            });
            _txtMessagePopUpNextActivity =
                _btnGlobePopUpNextActivity.transform.FindChild("TxtMessage").GetComponent<Text>();
            _popUpNextActivity.SetActive(true);
            //popUpIndication
            _popUpIndications = _challenge2.transform.GetChild(0).FindChild("PopUpIndications").gameObject;
            _txtMessagePopUpIndications = _popUpIndications.transform.FindChild("TxtMessage").GetComponent<Text>();
            _btnAceptPopUpIndications = _popUpIndications.transform.FindChild("BtnAcept").GetComponent<Button>();
            _btnAceptPopUpIndications.onClick.AddListener(delegate
            {
                PlaySoundClosePopUp();
                _popUpIndications.SetActive(false);
                PlaySoundOpenPopUp();
                _optionsPopUps.SetActive(true);
                _indexMessageOptionsPopUpsDisplay = Random.Range(_indexTxtMessagesButtonsMin,
                    _indexTxtMessagesButtonsMax);
                SetIndexMesssagesoptionsPopUpsAux();
                ShowOptionsPopUps(_indexMessageOptionsPopUpsDisplay, _indexMessagesOptionsPopUpsAux);
            });
            //get options popups questions
            _optionsPopUps = _challenge2.transform.GetChild(0).FindChild("OptionsPopUps").gameObject;
            _txtMessageCurrentOptionsPopUps = _optionsPopUps.transform.FindChild("TxtMessage").GetComponent<Text>();
            _btnAnswerA = _optionsPopUps.transform.FindChild("BtnAnswerA").GetComponent<Button>();
            _btnAnswerA.onClick.AddListener(delegate
            {
                PlaySoundClosePopUp();
                UpdateInfoPopUp(0);
                PlaySoundOpenPopUp();
            });
            _btnAnswerB = _optionsPopUps.transform.FindChild("BtnAnswerB").GetComponent<Button>();
            _btnAnswerB.onClick.AddListener(delegate
            {
                PlaySoundClosePopUp();
                UpdateInfoPopUp(1);
                PlaySoundOpenPopUp();
            });
            _btnAnswerC = _optionsPopUps.transform.FindChild("BtnAnswerC").GetComponent<Button>();
            _btnAnswerC.onClick.AddListener(delegate
            {
                PlaySoundClosePopUp();
                UpdateInfoPopUp(2);
                PlaySoundOpenPopUp();
            });
            //PopUpError
            _popUpError = _challenge2.transform.GetChild(0).FindChild("PopUpError").gameObject;
            _txtMessagePopUpError = _popUpError.transform.FindChild("TxtMessage").GetComponent<Text>();
            _btnAceptPopUpError = _popUpError.transform.FindChild("BtnAcept").GetComponent<Button>();
            _btnAceptPopUpError.onClick.AddListener(delegate
            {
                PlaySoundClosePopUp();
                _popUpError.SetActive(false);
                PlaySoundOpenPopUp();
                _optionsPopUps.SetActive(true);
                _indexTxtMessagesOptionsPopUps++;
                _indexMessageOptionsPopUpsDisplay = Random.Range(_indexTxtMessagesButtonsMin += 3,
                    _indexTxtMessagesButtonsMax += 3);
                SetIndexMesssagesoptionsPopUpsAux();
                ShowOptionsPopUps(_indexMessageOptionsPopUpsDisplay, _indexMessagesOptionsPopUpsAux);
            });
            //variables to pinatas
            _pinataHorse = _challenge2.transform.GetChild(0).FindChild("PinataHorse").GetComponent<Button>();
            _pinataHorse.onClick.AddListener(delegate { CheckValuesStars(0); });
            _pinataSpecial = _challenge2.transform.GetChild(0).FindChild("PinataSpecial").GetComponent<Button>();
            _pinataSpecial.onClick.AddListener(delegate { CheckValuesStars(1); });
            _pinataTradicional = _challenge2.transform.GetChild(0).FindChild("PinataTraditional").GetComponent<Button>();
            _pinataTradicional.onClick.AddListener(delegate
            {
                CheckValuesStars(2);
            });
            //variables to show popups questions to packages
            _screen9 = _challenge2.transform.GetChild(0).FindChild("Screen9").gameObject;
            _popUpErrorPackage = _screen9.transform.FindChild("PopUpError").gameObject;
            _txtMessagePopUpErrorPackage = _popUpErrorPackage.transform.FindChild("TxtMessage").GetComponent<Text>();
            _btnAceptPopUpErrorPackage = _popUpErrorPackage.transform.FindChild("BtnAcept").GetComponent<Button>();
            _btnAceptPopUpErrorPackage.onClick.AddListener(delegate
            {
                PlaySoundClosePopUp();
                _popUpErrorPackage.SetActive(false);
                PlaySoundOpenPopUp();
                _optionsPopUpsPackage.SetActive(true);
                ShowOptionsPopUpsActivity2(_indexMessagePackagesDisplay += 1, _indexMessagesPackagesAux += 3);
            });
            _optionsPopUpsPackage = _screen9.transform.FindChild("OptionsPopUps").gameObject;
            _txtMessageCurrentPackages = _optionsPopUpsPackage.transform.Find("TxtMessage").GetComponent<Text>();
            _btnAnswerAPackages = _optionsPopUpsPackage.transform.FindChild("BtnAnswerA").GetComponent<Button>();
            _btnAnswerAPackages.onClick.AddListener(delegate { UpdateInfoPopUpActivity2(0); });
            _btnAnswerBPackages = _optionsPopUpsPackage.transform.FindChild("BtnAnswerB").GetComponent<Button>();
            _btnAnswerBPackages.onClick.AddListener(delegate { UpdateInfoPopUpActivity2(1); });
            _btnAnswerCPackages = _optionsPopUpsPackage.transform.FindChild("BtnAnswerC").GetComponent<Button>();
            _btnAnswerCPackages.onClick.AddListener(delegate { UpdateInfoPopUpActivity2(2); });
            //popUpNextActivity 2
            _popUpNextActivity2 = _challenge2.transform.GetChild(0).FindChild("PopUpNextActivity2").gameObject;
            _btnGlobePopUpNextActivity2 =
                _popUpNextActivity2.transform.FindChild("PopUp")
                    .transform.FindChild("PopUpImage")
                    .GetComponent<Button>();
            _btnGlobePopUpNextActivity2.onClick.AddListener(ShowTextPopUpNextActivity2);
            _imgPackage1 = _screen9.transform.FindChild("Package1").gameObject;
            _imgPackage2 = _screen9.transform.FindChild("Package2").gameObject;
            _imgPackage3 = _screen9.transform.FindChild("Package3").gameObject;
            _imgPackage4 = _screen9.transform.FindChild("Package4").gameObject;
            _btnAceptPopUpNextActivity2 =
                _popUpNextActivity2.transform.FindChild("PopUp").transform.FindChild("BtnAcept").GetComponent<Button>();
            _btnAceptPopUpNextActivity2.onClick.AddListener(delegate
            {
                PlaySoundClosePopUp();
                _popUpNextActivity2.SetActive(false);
                PlaySoundOpenPopUp();
                _screen9.SetActive(true);
                Invoke("ShowPopUpActivity2Help", 2.0f);
            });
            _txtMessagePopUpNextActivity2 =
                _btnGlobePopUpNextActivity2.transform.FindChild("TxtMessage").GetComponent<Text>();
        }

        private void ShowPopUpActivity2Help()
        {
            _txtMessagePopUpNextActivity2.text = string.Empty;
            _optionsPopUpsPackage.SetActive(true);
            _banActiveOptionsPackages = Random.Range(0, 2);
            if (_banActiveOptionsPackages == 0)
            {
                _imgPackage4.transform.gameObject.SetActive(false);
                _imgPackage3.transform.gameObject.SetActive(false);
            }
            if (_banActiveOptionsPackages == 1)
            {
                _imgPackage1.transform.gameObject.SetActive(false);
                _imgPackage2.transform.gameObject.SetActive(false);
                _indexMessagePackagesDisplay = 6;
                _indexMessagesPackagesAux = 18;
            }
            ShowOptionsPopUpsActivity2(_indexMessagePackagesDisplay, _indexMessagesPackagesAux);
        }

        // Update is called once per frame
        internal void Update()
        {
            _txtScore.text = _starCountGame.ToString();
            //siempre al final de la animacion
            if (_animationStarted)
            {
                _timeAnimation -= Time.deltaTime;
                if (_timeAnimation <= 0)
                    _timeAnimation = 0;
                _txtMessagePopUpNextActivity.text = _text.Substring(0,
                    (int) ((_timeTotal - _timeAnimation)*_text.Length/_timeTotal));
                _txtMessagePopUpNextActivity2.text = _text.Substring(0,
                      (int) ((_timeTotal - _timeAnimation)*_text.Length/_timeTotal));
            }
        }

        private void SetUpValues()
        {
            //"estrellasODA12_ODA12" = "estrellasODA12_ODA12";
            _indexTxtMessagesButtonsMax = 3;
            _timeTxtMessagePopUpNextActivity = 9.0f;
            _timeTxtMessagePopUpNextActivity2 = 9.0f;
            //popUpNextActivity
            _stgMessagePopUpNextActivity =
                "Ahora, tengo que comprar una piñata y los dulces, pero no me salen las cuentas…";
            _banPopUpNextActivity = false;
            //popUpIndications
            _stgMessagePopUpIndications = new string[5];
            _stgMessagePopUpIndications[0] = " Ayúdale a sacar las cuentas y a escoger una piñata.";
            //questions
            _txtMessagesOptionsPopUps = new string[9];
            _txtMessagesButtonsOptionsPopUps = new string[27];
            _txtMessagesErrorsOptionsPopUps = new string[9];
            _optionsSuccessOptionsPopUps = new int[9];
            //option group random A1
            _txtMessagesOptionsPopUps[0] =
                "Si la abuela tiene 180 pesos y la piñata caballo cuesta 80 pesos, ¿cuánto dinero le va a quedar?";
            _txtMessagesButtonsOptionsPopUps[0] = "$ 80";
            _txtMessagesButtonsOptionsPopUps[1] = "$ 90";
            _txtMessagesButtonsOptionsPopUps[2] = "$ 100";
            _txtMessagesErrorsOptionsPopUps[0] = "180-80= 100, le quedan 100 pesos.";
            _optionsSuccessOptionsPopUps[0] = 2;
            //option group random A2
            _txtMessagesOptionsPopUps[1] =
                "Si la abuela tiene 230 pesos y la piñata caballo cuesta 80 pesos, ¿cuánto dinero le va a quedar?";
            _txtMessagesButtonsOptionsPopUps[3] = "$ 150";
            _txtMessagesButtonsOptionsPopUps[4] = "$ 160";
            _txtMessagesButtonsOptionsPopUps[5] = "$ 170";
            _txtMessagesErrorsOptionsPopUps[1] = "230-80= 150, le quedan 150 pesos.";
            _optionsSuccessOptionsPopUps[1] = 0;
            //option group random A3
            _txtMessagesOptionsPopUps[2] =
                "Si la abuela tiene 140 pesos y la piñata caballo cuesta 80 pesos, ¿cuánto dinero le va a quedar?";
            _txtMessagesButtonsOptionsPopUps[6] = "$ 50";
            _txtMessagesButtonsOptionsPopUps[7] = "$ 60";
            _txtMessagesButtonsOptionsPopUps[8] = "$ 70";
            _txtMessagesErrorsOptionsPopUps[2] = "140-80= 60, le quedan 60 pesos.";
            _optionsSuccessOptionsPopUps[2] = 1;
            //options group random b1
            _txtMessagesOptionsPopUps[3] =
                "Si la abuela tiene 180 pesos y la piñata tradicional cuesta 120 pesos, ¿cuánto dinero le va a quedar?";
            _txtMessagesButtonsOptionsPopUps[9] = "$ 50";
            _txtMessagesButtonsOptionsPopUps[10] = "$ 60";
            _txtMessagesButtonsOptionsPopUps[11] = "$ 70";
            _txtMessagesErrorsOptionsPopUps[3] = "180-120= 60, le quedan 60 pesos.";
            _optionsSuccessOptionsPopUps[3] = 1;
            //options group random b2
            _txtMessagesOptionsPopUps[4] =
                "Si la abuela tiene 230 pesos y la piñata tradicional cuesta 120 pesos, ¿cuánto dinero le va a quedar?";
            _txtMessagesButtonsOptionsPopUps[12] = "$ 80";
            _txtMessagesButtonsOptionsPopUps[13] = "$ 90";
            _txtMessagesButtonsOptionsPopUps[14] = "$ 110";
            _txtMessagesErrorsOptionsPopUps[4] = "230-120= 110, le quedan 110 pesos.";
            _optionsSuccessOptionsPopUps[4] = 2;
            //options group random b3
            _txtMessagesOptionsPopUps[5] =
                "Si la abuela tiene 140 pesos y la piñata tradicional cuesta 120 pesos, ¿cuánto dinero le va a quedar?";
            _txtMessagesButtonsOptionsPopUps[15] = "$ 10";
            _txtMessagesButtonsOptionsPopUps[16] = "$ 20";
            _txtMessagesButtonsOptionsPopUps[17] = "$ 30";
            _txtMessagesErrorsOptionsPopUps[5] = "140-120= 20, le quedan 20 pesos.";
            _optionsSuccessOptionsPopUps[5] = 1;
            //option group random c1
            _txtMessagesOptionsPopUps[6] =
                "Ahora, si la abuela tiene 185 pesos y la piñata especial cuesta 100 pesos, ¿cuánto dinero le va a quedar?";
            _txtMessagesButtonsOptionsPopUps[18] = "$ 65";
            _txtMessagesButtonsOptionsPopUps[19] = "$ 75";
            _txtMessagesButtonsOptionsPopUps[20] = "$ 85";
            _txtMessagesErrorsOptionsPopUps[6] = "185-100= 85, le quedan 85 pesos.";
            _optionsSuccessOptionsPopUps[6] = 2;
            //option group random c2
            _txtMessagesOptionsPopUps[7] =
                "Ahora, si la abuela tiene 235 pesos y la piñata especial cuesta 100 pesos, ¿cuánto dinero le va a quedar?";
            _txtMessagesButtonsOptionsPopUps[21] = "$ 115";
            _txtMessagesButtonsOptionsPopUps[22] = "$ 135";
            _txtMessagesButtonsOptionsPopUps[23] = "$ 215";
            _txtMessagesErrorsOptionsPopUps[7] = "235 -100= 115, le quedan 135 pesos.";
            _optionsSuccessOptionsPopUps[7] = 1;
            //option group random c3
            _txtMessagesOptionsPopUps[8] =
                "Ahora, si la abuela tiene 143 pesos y la piñata especial cuesta 100 pesos, ¿cuánto dinero le va a quedar?";
            _txtMessagesButtonsOptionsPopUps[24] = "$ 43";
            _txtMessagesButtonsOptionsPopUps[25] = "$ 63";
            _txtMessagesButtonsOptionsPopUps[26] = "$ 73";
            _txtMessagesErrorsOptionsPopUps[8] = "143-100= 43, le quedan 43 pesos.";
            _optionsSuccessOptionsPopUps[8] = 0;
            //msg final
            _stgMessagePopUpIndications[1] =
                "¡Gran noticia! Con las estrellas que reuniste vas a poder escoger la piñata.";
            _stgMessagePopUpIndications[2] = "¡Muy bien! ¡Tenemos piñata para la fiesta!";
            _stgMessagePopUpNextActivity2 = "Aún me faltan los dulces, ¿me ayudarías a escoger un paquete?";
            //activity 2
            _txtMessagesPackages = new string[12];
            _txtMessagesButtonsPackages = new string[36];
            _txtMessagesErrorsPackages = new string[12];
            _optionsSuccessPackages = new int[12];
            //package 1.1
            _txtMessagesPackages[0] = "Una paleta cuesta un peso. ¿Cuánto le van a costar 60 paletas?";
            _txtMessagesButtonsPackages[0] = "$ 50";
            _txtMessagesButtonsPackages[1] = "$ 60";
            _txtMessagesButtonsPackages[2] = "$ 70";
            _txtMessagesErrorsPackages[0] =
                "Si una paleta cuesta 1 peso, hay que multiplicar el precio por el número de paletas que se quiere comprar: 1x60= 60.  Las paletas cuestan 60 pesos.";
            _optionsSuccessPackages[0] = 1;
            //package 1.2
            _txtMessagesPackages[1] = "Un mazapán cuesta 2 pesos. ¿Cuánto le van a costar 20 mazapanes?";
            _txtMessagesButtonsPackages[3] = "$ 40";
            _txtMessagesButtonsPackages[4] = "$ 42";
            _txtMessagesButtonsPackages[5] = "$ 50";
            _txtMessagesErrorsPackages[1] =
                "Si 1 mazapán cuesta 2 pesos, hay que multiplicar el precio por el número de mazapanes que se quiere comprar: 2x20= 40. Los mazapanes cuestan 40 pesos.";
            _optionsSuccessPackages[1] = 0;
            //package 1.3
            _txtMessagesPackages[2] =
                "El paquete 1 contiene 60 paletas que cuestan 60 pesos y 20 mazapanes que cuestan 40 pesos, entonces el paquete cuesta: ";
            _txtMessagesButtonsPackages[6] = "$ 90";
            _txtMessagesButtonsPackages[7] = "$ 100";
            _txtMessagesButtonsPackages[8] = "$ 110";
            _txtMessagesErrorsPackages[2] =
                "Las 60 paletas cuestan 60 pesos y los 20 mazapanes cuestan 40 pesos. Hay que sumar los dos para obtener el resultado: 60+40= 100. El paquete cuesta 100 pesos.";
            _optionsSuccessPackages[2] = 1;
            //package 2.1
            _txtMessagesPackages[3] = " Una barra de chocolate cuesta 4 pesos, ¿en cuánto salen 8 barras?";
            _txtMessagesButtonsPackages[9] = "$ 32";
            _txtMessagesButtonsPackages[10] = "$ 35";
            _txtMessagesButtonsPackages[11] = "$ 40";
            _txtMessagesErrorsPackages[3] =
                "Si una barra de chocolate cuesta 4 pesos, hay que multiplicar el precio por el número de barras que se quiere comprar: 4x8= 32. Las barras de chocolate le van a costar 32 pesos.";
            _optionsSuccessPackages[3] = 0;
            //package 2.2
            _txtMessagesPackages[4] = "Un dulce de tamarindo cuesta 3 pesos, ¿en cuánto salen 16  dulces de tamarindo?";
            _txtMessagesButtonsPackages[12] = "$ 36";
            _txtMessagesButtonsPackages[13] = "$ 48";
            _txtMessagesButtonsPackages[14] = "$ 58";
            _txtMessagesErrorsPackages[4] =
                "Si un dulce de tamarindo cuesta 3 pesos, hay que multiplicar el precio por el número de dulces de tamarindo que se quiere comprar:  3x16=48. Los dulces le van a costar 48 pesos. ";
            _optionsSuccessPackages[4] = 1;
            //package 2.3
            //popup extra 5
            _txtMessagesPackages[5] =
                "El paquete 2 contiene 8 barras de chocolate que cuestan 32 pesos y 16 tamarindos que cuestan 48 pesos, entonces el paquete cuesta:";
            _txtMessagesButtonsPackages[15] = "$ 38";
            _txtMessagesButtonsPackages[16] = "$ 80";
            _txtMessagesButtonsPackages[17] = "$ 76";
            _txtMessagesErrorsPackages[5] =
                "Las 8 barras de chocolate cuestan 32 pesos y los 16 tamarindos cuestan 48 pesos. Hay que sumar los dos para obtener el resultado:  32+48= 80. El paquete 2 cuesta 80 pesos.";
            _optionsSuccessPackages[5] = 1;
            //package3.1
            _txtMessagesPackages[6] = "Un paleta cuesta un peso. ¿Cuánto le van a costar 35 paletas?";
            _txtMessagesButtonsPackages[18] = "$ 30";
            _txtMessagesButtonsPackages[19] = "$ 35";
            _txtMessagesButtonsPackages[20] = "$ 45";
            _txtMessagesErrorsPackages[6] =
                "Si una paleta cuesta 1 peso, hay que multiplicar el precio por el número de paletas que se quiere comprar: 1x35= 35. Las paletas le van a costar 35 pesos.";
            _optionsSuccessPackages[6] = 1;
            //package 3.2
            _txtMessagesPackages[7] =
                "Un dulce de tamarindo cuesta 3 pesos. ¿Cuánto le van a costar 25 dulces de tamarindo?";
            _txtMessagesButtonsPackages[21] = "$ 75";
            _txtMessagesButtonsPackages[22] = "$ 85";
            _txtMessagesButtonsPackages[23] = "$ 95";
            _txtMessagesErrorsPackages[7] =
                "Si un dulce de tamarindo cuesta 3 pesos, hay que multiplicar el precio por el número de tamarindos que se quiere comprar: 3x25= 75. Los dulces de tamarindo cuestan 75 pesos.";
            _optionsSuccessPackages[7] = 0;
            //package 3.3
            _txtMessagesPackages[8] =
                "El paquete 1 contiene 35 paletas que cuestan 35 pesos y 25 dulces de tamarindo que cuestan 75 pesos, entonces el paquete cuesta: ";
            _txtMessagesButtonsPackages[24] = "$ 110";
            _txtMessagesButtonsPackages[25] = "$ 115";
            _txtMessagesButtonsPackages[26] = "$ 120";
            _txtMessagesErrorsPackages[8] =
                "Las 35 paletas cuestan 35 pesos y los 25 dulces de tamarindo cuestan 75 pesos. Hay que sumar los dos para obtener el resultado. 35+75= 110. El paquete cuesta 110 pesos.";
            _optionsSuccessPackages[8] = 0;
            //package 4.1
            _txtMessagesPackages[9] = " Una barra de chocolate cuesta 4 pesos, ¿en cuánto salen 10 barras?";
            _txtMessagesButtonsPackages[27] = "$ 14";
            _txtMessagesButtonsPackages[28] = "$ 35";
            _txtMessagesButtonsPackages[29] = "$ 40";
            _txtMessagesErrorsPackages[9] =
                "Si una barra de chocolate cuesta 4 pesos, hay que multiplicar el precio por le número de barras de chocolate que se quiere comprar: 4x10= 40. Las barras de chocolate cuestan 40 pesos.";
            _optionsSuccessPackages[9] = 2;
            //package 4.2
            _txtMessagesPackages[10] = "Un mazapán cuesta 2 pesos, ¿en cuánto salen 36 mazapanes?";
            _txtMessagesButtonsPackages[30] = "$ 62";
            _txtMessagesButtonsPackages[31] = "$ 72";
            _txtMessagesButtonsPackages[32] = "$ 82";
            _txtMessagesErrorsPackages[10] =
                "Si un mazapán cuesta 2 pesos, hay que multiplicar el precio por el número de mazapanes que se quiere comprar: 2x36=72. Los mazapanes cuestan 72 pesos. ";
            _optionsSuccessPackages[10] = 1;
            //package 4.3
            //pop up extra 11
            _txtMessagesPackages[11] =
                "El paquete 2 contiene 10 barras de chocolate que cuestan 40 pesos y 36 mazapanes que cuestan 72 pesos, ¿entonces cuánto cuesta el paquete?";
            _txtMessagesButtonsPackages[33] = "$ 112";
            _txtMessagesButtonsPackages[34] = "$ 113";
            _txtMessagesButtonsPackages[35] = "$ 114";
            _txtMessagesErrorsPackages[11] =
                "Las 10 barras de chocolate cuestan 40 pesos y los 36 mazapanes cuestan 72 pesos. Hay que sumar los dos para obtener el resultado: 40+72= 112. El paquete 2 cuesta 112 pesos.";
            _optionsSuccessPackages[11] = 0;

            _stgMessagePopUpIndications[3] =
                "No cuentas con suficientes estrellas. Vuelve a intentar el reto 1 para sumar más estrellas.";
            _stgMessagePopUpIndications[4] = "No cuentas con suficientes estrellas. Elige otra piñata.";
        }

        private void ShowTextPopUpNextActivity()
        {
            if (_banPopUpNextActivity) return;
            //AnimatedString(_timeTxtMessagePopUpNextActivity, " ");
            AnimatedString(_timeTxtMessagePopUpNextActivity, _stgMessagePopUpNextActivity);
            Invoke("ShowPopUpIndicationsHelp", _timeTxtMessagePopUpNextActivity + 0.5f);
            _banPopUpNextActivity = true;
        }

        private void SetEnableBtnAceptNextActivity()
        {
            PlaySoundOpenPopUp();
            _btnAceptPopUpNextActivity.gameObject.SetActive(true);
        }

        public void AnimatedString(float time, string t)
        {
            _text = t;
            _timeAnimation = time;
            _timeTotal = _timeAnimation;
            _animationStarted = true;
        }

        private void ShowPopUpIndicationsHelpTwoSeconds()
        {
            ShowPopUpIndications(0);
        }

        private void ShowPopUpIndicationsHelp()
        {
            PlaySoundClosePopUp();
            _popUpNextActivity.SetActive(false);
            Invoke("ShowPopUpIndicationsHelpTwoSeconds",2.0f);
        }

        private void ShowPopUpIndications(int indexMessage)
        {
            PlaySoundOpenPopUp();
            _popUpIndications.SetActive(true);
            _txtMessagePopUpIndications.text = _stgMessagePopUpIndications[indexMessage];
        }

        private void SetIndexMesssagesoptionsPopUpsAux()
        {
            switch (_indexMessageOptionsPopUpsDisplay)
            {
                //group a
                case 0:
                    _indexMessagesOptionsPopUpsAux = 0;
                    break;
                case 1:
                    _indexMessagesOptionsPopUpsAux = 3;
                    break;
                case 2:
                    _indexMessagesOptionsPopUpsAux = 6;
                    break;
                //group b
                case 3:
                    _indexMessagesOptionsPopUpsAux = 9;
                    break;
                case 4:
                    _indexMessagesOptionsPopUpsAux = 12;
                    break;
                case 5:
                    _indexMessagesOptionsPopUpsAux = 15;
                    break;
                //group c
                case 6:
                    _indexMessagesOptionsPopUpsAux = 18;
                    break;
                case 7:
                    _indexMessagesOptionsPopUpsAux = 21;
                    break;
                case 8:
                    _indexMessagesOptionsPopUpsAux = 24;
                    break;
            }
        }

        private void ShowOptionsPopUps(int indexMessagesCurrent, int indexMessagesButtons)
        {
            _txtMessageCurrentOptionsPopUps.text = _txtMessagesOptionsPopUps[indexMessagesCurrent];
            _btnAnswerA.transform.FindChild("TxtAnswer").GetComponent<Text>().text =
                _txtMessagesButtonsOptionsPopUps[indexMessagesButtons];
            _btnAnswerB.transform.FindChild("TxtAnswer").GetComponent<Text>().text =
                _txtMessagesButtonsOptionsPopUps[indexMessagesButtons + 1];
            _btnAnswerC.transform.FindChild("TxtAnswer").GetComponent<Text>().text =
                _txtMessagesButtonsOptionsPopUps[indexMessagesButtons + 2];
            _txtMessagePopUpError.text = _txtMessagesErrorsOptionsPopUps[indexMessagesCurrent];
        }

        private void UpdateInfoPopUp(int option)
        {
            if (_indexTxtMessagesOptionsPopUps < 2)
            {
                if (option != _optionsSuccessOptionsPopUps[_indexMessageOptionsPopUpsDisplay])
                {
                    PlaySoundError();
                    PlaySoundClosePopUp();
                    _optionsPopUps.SetActive(false);
                    PlaySoundOpenPopUp();
                    _popUpError.SetActive(true);
                }
                else
                {
                    PlaySoundSuccess();
                    _starCountGame += 2;
                    _indexTxtMessagesOptionsPopUps++;
                    _indexMessageOptionsPopUpsDisplay = Random.Range(_indexTxtMessagesButtonsMin += 3,
                        _indexTxtMessagesButtonsMax += 3);
                    SetIndexMesssagesoptionsPopUpsAux();
                    ShowOptionsPopUps(_indexMessageOptionsPopUpsDisplay, _indexMessagesOptionsPopUpsAux);
                }
            }
            else if (_indexTxtMessagesOptionsPopUps >= 2)
            {
                if (option != _optionsSuccessOptionsPopUps[_indexMessageOptionsPopUpsDisplay])
                {
                    PlaySoundError();
                    PlaySoundClosePopUp();
                    _optionsPopUps.SetActive(false);
                    PlaySoundOpenPopUp();
                    _popUpError.SetActive(true);
                    _btnAceptPopUpError.onClick.RemoveAllListeners();
                    _btnAceptPopUpError.onClick.AddListener(delegate
                    {
                        PlaySoundClosePopUp();
                        _optionsPopUps.SetActive(false);
                        _popUpError.SetActive(false);
                        ShowPopUpIndications(1);
                    });
                }
                else
                {
                    PlaySoundSuccess();
                    _starCountGame += 2;
                    PlaySoundClosePopUp();
                    _optionsPopUps.SetActive(false);
                    ShowPopUpIndications(1);
                }
                _btnAceptPopUpIndications.onClick.RemoveAllListeners();
                _btnAceptPopUpIndications.onClick.AddListener(delegate
                {
                    _popUpIndications.SetActive(false);
                    PlaySoundClosePopUp();
                    _challenge2.transform.GetChild(0)
                        .FindChild("pricePinataHorse")
                        .transform.gameObject.SetActive(false);
                    _challenge2.transform.GetChild(0)
                        .FindChild("pricePinataSpecial")
                        .transform.gameObject.SetActive(false);
                    _challenge2.transform.GetChild(0)
                        .FindChild("pricePinataTraditional")
                        .transform.gameObject.SetActive(false);
                });
            }
            PlayerPrefs.SetInt("estrellasODA12_ODA12", _starCountGame);
        }

        public void CheckValuesStars(int indexPinata)
        {
            bool banChoose = false;
            switch (indexPinata)
            {
                case 0:
                    //16
                    if (_starCountGame >= 16)
                    {
                        PlayerPrefs.SetString("PinataName", "PinataHorse");
                        _starCountGame = _starCountGame - 16;
                        banChoose = true;
                    }
                    break;
                case 1:
                    //20
                    if (_starCountGame >= 4)
                    {
                        PlayerPrefs.SetString("PinataName", "PinataSpecial");
                        _starCountGame = _starCountGame - 4;
                        banChoose = true;
                    }
                    break;
                case 2:
                    //24
                    if (_starCountGame >= 8)
                    {
                        PlayerPrefs.SetString("PinataName", "PinataTraditional");
                        _starCountGame = _starCountGame - 8;
                        banChoose = true;
                    }
                    break;
            }
            PlayerPrefs.SetInt("estrellasODA12_ODA12", _starCountGame);
            if (banChoose)
            {
                PlaySoundPinata();
                ShowPopUpIndications(2);
                _btnAceptPopUpIndications.onClick.RemoveAllListeners();
                _btnAceptPopUpIndications.onClick.AddListener(delegate
                {
                    PlaySoundClosePopUp();
                    _popUpIndications.SetActive(false);
                    ShowTextPopUpNextActivity2();
                });
            }
            else
            {
                if (_starCountGame < 4)
                {
                    ShowPopUpIndications(3);
                }
                else
                {
                    ShowPopUpIndications(4);
                }
            }
        }

        private void ShowTextPopUpNextActivity2()
        {
            if (_banPopUpNextActivity2) return;
            AnimatedString(_timeTxtMessagePopUpNextActivity, "");
            PlaySoundOpenPopUp();
            _popUpNextActivity2.SetActive(true);
            AnimatedString(_timeTxtMessagePopUpNextActivity2, _stgMessagePopUpNextActivity2);
            Invoke("SetEnableBtnAceptActivity2", _timeTxtMessagePopUpNextActivity2 + 0.5f);
            _banPopUpNextActivity2 = true;
        }

        private void SetEnableBtnAceptActivity2()
        {
            _btnAceptPopUpNextActivity2.gameObject.SetActive(true);
        }

        private IEnumerator Wait()
        {
            yield return new WaitForSeconds(2.0f);
            _optionsPopUpsPackage.SetActive(true);
        }

        private void ShowOptionsPopUpsActivity2(int indexMessagesCurrent, int indexMessagesButtons)
        {
            if (indexMessagesCurrent == 3||indexMessagesCurrent==9)
            {
                _optionsPopUpsPackage.SetActive(false);
                _imgPackage1.SetActive(false);
                _imgPackage3.SetActive(false);
                StartCoroutine(Wait());
            }
            _txtMessageCurrentPackages.text = _txtMessagesPackages[indexMessagesCurrent];
            _btnAnswerAPackages.transform.FindChild("TxtAnswer").GetComponent<Text>().text =
                _txtMessagesButtonsPackages[indexMessagesButtons];
            _btnAnswerBPackages.transform.FindChild("TxtAnswer").GetComponent<Text>().text =
                _txtMessagesButtonsPackages[indexMessagesButtons + 1];
            _btnAnswerCPackages.transform.FindChild("TxtAnswer").GetComponent<Text>().text =
                _txtMessagesButtonsPackages[indexMessagesButtons + 2];
            if (_txtMessagesErrorsPackages.Length > indexMessagesCurrent)
                _txtMessagePopUpErrorPackage.text = _txtMessagesErrorsPackages[indexMessagesCurrent];
        }

        private void UpdateInfoPopUpActivity2(int option)
        {
            if (_banActiveOptionsPackages == 0)
            {
                if (_indexMessagePackagesDisplay < 5)
                {
                    if (option != _optionsSuccessPackages[_indexMessagePackagesDisplay])
                    {
                        PlaySoundClosePopUp();
                        _optionsPopUpsPackage.SetActive(false);
                        PlaySoundOpenPopUp();
                        _popUpErrorPackage.SetActive(true);
                        PlaySoundError();
                    }
                    else
                    {
                        _starCountGame += 2;
                        PlaySoundSuccess();
                        ShowOptionsPopUpsActivity2(_indexMessagePackagesDisplay += 1, _indexMessagesPackagesAux += 3);
                    }
                }
                else if (_indexMessagePackagesDisplay >= 5)
                {
                    if (option != _optionsSuccessPackages[_indexMessagePackagesDisplay])
                    {
                        PlaySoundError();
                        PlaySoundClosePopUp();
                        _optionsPopUpsPackage.SetActive(false);
                        PlaySoundOpenPopUp();
                        _popUpErrorPackage.SetActive(true);
                           _btnAceptPopUpErrorPackage.onClick.RemoveAllListeners();
                        _btnAceptPopUpErrorPackage.onClick.AddListener(delegate
                        {
                            PlaySoundClosePopUp();
                            _optionsPopUpsPackage.SetActive(false);
                            _popUpErrorPackage.SetActive(false);
                            PlaySoundOpenPopUp();
                            _popUpIndications.SetActive(true);
                        });
                    }
                    else
                    {
                        PlaySoundSuccess();
                        _starCountGame += 2;
                        PlaySoundClosePopUp();
                        _optionsPopUpsPackage.SetActive(false);
                        _popUpIndications.SetActive(true);
                    }
                    _btnAceptPopUpIndications.onClick.RemoveAllListeners();
                    _btnAceptPopUpIndications.onClick.AddListener(EndChallenge);
                    _txtMessagePopUpIndications.text =
                        "La abuela decidió llevarse los dos paquetes. ¡Sigamos con el tercer reto!";
                }
            }
            else
            {
                if (_indexMessagePackagesDisplay < 11)
                {
                    if (option != _optionsSuccessPackages[_indexMessagePackagesDisplay])
                    {
                        PlaySoundError();
                        PlaySoundOpenPopUp();
                        _optionsPopUpsPackage.SetActive(false);
                        PlaySoundClosePopUp();
                        _popUpErrorPackage.SetActive(true);
                    }
                    else
                    {
                        PlaySoundSuccess();
                        _starCountGame += 2;
                        ShowOptionsPopUpsActivity2(_indexMessagePackagesDisplay += 1, _indexMessagesPackagesAux += 3);
                    }
                }
                else if (_indexMessagePackagesDisplay >= 11)
                {
                    if (option != _optionsSuccessPackages[_indexMessagePackagesDisplay])
                    {
                        PlaySoundError();
                        PlaySoundClosePopUp();
                        _optionsPopUpsPackage.SetActive(false);
                        PlaySoundOpenPopUp();
                        _popUpErrorPackage.SetActive(true);
                        _btnAceptPopUpErrorPackage.onClick.RemoveAllListeners();
                        _btnAceptPopUpErrorPackage.onClick.AddListener(delegate
                        {
                            PlaySoundClosePopUp();
                            _optionsPopUpsPackage.SetActive(false);
                            _popUpErrorPackage.SetActive(false);
                            PlaySoundOpenPopUp();
                            _popUpIndications.SetActive(true);
                        });
                    }
                    else
                    {
                        PlaySoundSuccess();
                        _starCountGame += 2;
                        PlaySoundClosePopUp();
                        _optionsPopUpsPackage.SetActive(false);
                        _popUpIndications.SetActive(true);
                    }
                    _btnAceptPopUpIndications.onClick.RemoveAllListeners();
                    _btnAceptPopUpIndications.onClick.AddListener(EndChallenge);
                    _txtMessagePopUpIndications.text =
                        "La abuela decidió llevarse los dos paquetes. ¡Sigamos con el tercer reto!";
                }
            }
            PlayerPrefs.SetInt("estrellasODA12_ODA12", _starCountGame);
        }

        private void EndChallenge()
        {
            PlaySoundClosePopUp();
            _popUpIndications.SetActive(false);
            _optionsPopUpsPackage.SetActive(false);
            _challenge2.SetActive(false);
            PlaySoundEndChallenge();
            _challenge3.SetActive(true);    
        }

        private void PlaySoundSuccess()
        {
            _soundManager.PlaySound("Sound_success", 1);
        }

        private void PlaySoundError()
        {
            _soundManager.PlaySound("Sound_error", 2);
        }

        private void PlaySoundOpenPopUp()
        {
            _soundManager.PlaySound("Sound_openPopUp", 3);
        }

        private void PlaySoundClosePopUp()
        {
            _soundManager.PlaySound("Sound_closePopUp", 4);
        }

        private void PlaySoundPinata()
        {
            _soundManager.PlaySound("Sound_pinata", 4);
        }

        private void PlaySoundEndChallenge()
        {
            _soundManager.PlaySound("Sound_endChallenge", 2);
        }
    }
}